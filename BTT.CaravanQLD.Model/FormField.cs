﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class FormField
    {
        public FormField()
        {
            Id = Guid.NewGuid();
            DataType = FormDataType.TextBox;
            Label = "Label";
            Name = "New Field";
            TextContent = "";
            Order = 0;
            FormFieldValues = new List<FormFieldValue>();
            FormFieldItems = new List<FormFieldItem>();
        }

        [Key]
        public Guid Id { get; set; }
        public FormDataType DataType { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string TextContent { get; set; }
        public int Order { get; set; }
        public bool Mandatory { get; set; }
        public bool SpanTwoColumns { get; set; }

        [Required]
        public Guid FormId { get; set; }
        [Column("FormId")]
        public virtual Form Form { get; set; }

        public virtual ICollection<FormFieldValue> FormFieldValues { get; set; }
        public virtual ICollection<FormFieldItem> FormFieldItems { get; set; }
    }
}
