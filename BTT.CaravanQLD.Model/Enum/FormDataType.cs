﻿using System.ComponentModel.DataAnnotations;
public enum FormDataType
{
    //Display Only
    [Display(Name = "Content Section")]
    ContentSection = 0,
    //Simple
    [Display(Name="Text Box")]
    TextBox = 1,
    [Display(Name = "File Attachment")]
    FileAttachment = 2,
    [Display(Name = "Text Area")]
    TextArea = 7,
    //Complex - These need Form Field Items
    [Display(Name = "Drop Down List")]
    DropDown = 3,
    [Display(Name = "Checkbox")]
    Checkbox = 4,
    [Display(Name = "Checkbox Array")]
    CheckboxArray = 5,
    [Display(Name = "Radio Buttons")]
    RadioButton = 6
}