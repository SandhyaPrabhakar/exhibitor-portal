﻿using System.ComponentModel.DataAnnotations;
public enum PageType
{
    [Display(Name = "Custom Content")]
    Custom = 0,
    [Display(Name = "Login Content")]
    Login = 3,
    [Display(Name = "Exhibitor Portal Content")]
    ExhibitorPortal = 4
}