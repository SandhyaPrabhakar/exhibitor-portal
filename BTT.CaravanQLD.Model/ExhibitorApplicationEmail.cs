﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("ExhibitorApplicationEmail")]
    public class ExhibitorApplicationEmail
    {
        [Key, Column(Order=0)]
        public Guid ExhibitorApplicationId { get; set; }
        [Key, Column(Order = 1)]
        public Guid EmailId { get; set; }
        [Column("ExhibitorApplicationId")]
        public ExhibitorApplication ExhibitorApplication { get; set; }
        [Column("EmailId")]
        public Email Email { get; set; }
    }
}
