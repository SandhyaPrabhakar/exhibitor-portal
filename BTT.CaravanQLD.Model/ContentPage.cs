﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class ContentPage
    {
        public ContentPage()
        {
            Id = Guid.NewGuid();
            PageType = PageType.Custom;
            Name = "New Page";
            TextContent = "";
            Order = 0;
        }

        [Key]
        public Guid Id { get; set; }
        public PageType PageType { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string TextContent { get; set; }
        public int Order { get; set; }
        public bool IsCurrent { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }

        public string LinkName
        {
            get
            {
                return Name.Replace(" ", "_").Replace("&", "amp1");
            }
        }
    }
}
