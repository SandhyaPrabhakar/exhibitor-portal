﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class Show
    {
        public Show()
        {
            Id = Guid.NewGuid();
            ShowType = ShowType.Main;
            Name = "New Show";
            StartDate = DateTime.Now;
            EndDate = DateTime.Now.AddMonths(6);
            LinkIdentifier = "Show" + new Random().Next(1000, 9999);
        }

        [Key]
        public Guid Id { get; set; }
        public ShowType ShowType { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string LinkIdentifier { get; set; }

        public virtual ICollection<ContentPage> ContentPages { get; set; }
        public virtual ICollection<Email> Emails { get; set; }
        public virtual ICollection<EmailLog> EmailLogs { get; set; }
        public virtual ICollection<EmailSetting> EmailSettings { get; set; }
        public virtual ICollection<ExhibitorApplication> ExhibitorApplications { get; set; }
        public virtual ICollection<FormGroup> FormGroups { get; set; }
        public virtual ICollection<Form> Forms { get; set; }
    }
}
