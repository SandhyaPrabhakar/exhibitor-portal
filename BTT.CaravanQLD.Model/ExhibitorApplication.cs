﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("ExhibitorApplication")]
    public class ExhibitorApplication
    {
        public ExhibitorApplication()
        {
            Id = Guid.NewGuid();
            TradingName = "";
            ShowListingName = "";
            AddressLine1 = "";
            AddressLine2 = "";
            Suburb = "";
            State = "";
            Country = "";
            Postcode = "";
            ExhibitorPhone = "";
            ExhibitorFax = "";
            ExhibitorEmail = "";
            Website = "";
            Title = "";
            FirstName = "";
            Surname = "";
            Mobile = "";
            Phone = "";
            Email = "";
            AllocatedStandNumber = "";
            SiteLocation = "";
            Frontage = "";
            Depth = "";
            Area = "";
            MoveInDate = "";
            AccessGate = "";
            Notes = "";
            AccessTime = "";

            EmailLogs = new List<EmailLog>();
            ExhibitorApplicationEmails = new List<ExhibitorApplicationEmail>();
            FormFieldValues = new List<FormFieldValue>();
            FormExhibitors = new List<FormExhibitor>();
        }

        //Exhibitor Details
        [Key]
        public Guid Id { get; set; }
        [Display(Name = "Show ID")]
        public string ShowId { get; set; }
        [MaxLength(150)]
        public string TradingName { get; set; }
        [MaxLength(150)]
        public string ShowListingName { get; set; }
        [MaxLength(150)]
        public string AddressLine1 { get; set; }
        [MaxLength(150)]
        public string AddressLine2 { get; set; }
        [MaxLength(50), Display(Name = "Suburb / Town")]
        public string Suburb { get; set; }
        [MaxLength(50)]
        public string State { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
        [MaxLength(10)]
        public string Postcode { get; set; }
        [MaxLength(20)]
        public string ExhibitorPhone { get; set; }
        [MaxLength(20)]
        public string ExhibitorFax { get; set; }
        [MaxLength(100)]
        public string ExhibitorEmail { get; set; }
        [MaxLength(100)]
        public string Website { get; set; }

        //Contact Person Details
        [MaxLength(50), Display(Name = "Title / Position")]
        public string Title { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string Surname { get; set; }
        [MaxLength(20)]
        public string Mobile { get; set; }
        [MaxLength(20)]
        public string Phone { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        public string AllocatedStandNumber { get; set; }
        public string SiteLocation { get; set; }
        public string Frontage { get; set; }
        public string Depth { get; set; }
        public string Area { get; set; }
        public string MoveInDate { get; set; }
        public string AccessGate { get; set; }
        public string Notes { get; set; }
        public string AccessTime { get; set; }

        public DateTime? DateAdded { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }

        public virtual ICollection<EmailLog> EmailLogs { get; set; }
        public virtual ICollection<ExhibitorApplicationEmail> ExhibitorApplicationEmails { get; set; }
        public virtual ICollection<FormFieldValue> FormFieldValues { get; set; }
        public virtual ICollection<FormExhibitor> FormExhibitors { get; set; }

    }
}
