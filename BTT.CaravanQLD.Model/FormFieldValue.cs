﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class FormFieldValue
    {
        public FormFieldValue()
        {
            Id = Guid.NewGuid();
            Value = "";
        }

        [Key]
        public Guid Id { get; set; }
        public string Value { get; set; }

        [Required]
        public Guid FormFieldId { get; set; }
        [Column("FormFieldId")]
        public virtual FormField FormField { get; set; }

        [Required]
        public Guid ExhibitorApplicationId { get; set; }
        [Column("ExhibitorApplicationId")]
        public virtual ExhibitorApplication ExhibitorApplication { get; set; }
    }
}
