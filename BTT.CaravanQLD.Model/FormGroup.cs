﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class FormGroup
    {
        public FormGroup()
        {
            Id = Guid.NewGuid();
            Name = "New Form Group";
            Order = 0;
            Forms = new List<Form>();
        }

        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }

        public virtual ICollection<Form> Forms { get; set; }
    }
}
