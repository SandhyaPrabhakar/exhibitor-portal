﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class FormExhibitor
    {
        public FormExhibitor()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }
        public DateTime? DateCompleted { get; set; }

        [Required]
        public Guid FormId { get; set; }
        [Column("FormId")]
        public virtual Form Form { get; set; }

        [Required]
        public Guid ExhibitorApplicationId { get; set; }
        [Column("ExhibitorApplicationId")]
        public virtual ExhibitorApplication ExhibitorApplication { get; set; }
    }
}
