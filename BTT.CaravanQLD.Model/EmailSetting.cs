﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("EmailSetting")]
    public class EmailSetting
    {
        public EmailSetting()
        {
            Id = Guid.NewGuid();
            LogsPerPage = 25;
        }

        [Key]
        public Guid Id { get; set; }
        public string CompanyName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactNumber { get; set; }
        public string ContactFax { get; set; }
        public string ContactPostalAddress { get; set; }
        public string Website { get; set; }
        public string ShowTitle { get; set; }
        public string TestEmailAddress { get; set; }
        public string TestExhibitorShowID { get; set; }
        //True is By Date Created
        //False is By Subject (alphabetically)
        public bool SortByDateCreated { get; set; }
        public int LogsPerPage { get; set; }
        public string AdminEmailAddress { get; set; }
        public string EmailSender { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }
    }
}
