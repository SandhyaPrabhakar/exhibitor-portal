﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class FormFieldItem
    {
        public FormFieldItem()
        {
            Id = Guid.NewGuid();
            Text = "New";
            Value = "New";
            Order = 0;
        }

        [Key]
        public Guid Id { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public int Order { get; set; }

        [Required]
        public Guid FormFieldId { get; set; }
        [Column("FormFieldId")]
        public virtual FormField FormField { get; set; }
    }
}
