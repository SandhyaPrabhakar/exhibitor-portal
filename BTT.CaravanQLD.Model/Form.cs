﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class Form
    {
        public Form()
        {
            Id = Guid.NewGuid();
            Name = "New Form";
            Order = 0;
            DueDate = DateTime.Now.Date;
            Type = FormType.Standard;
            IsCurrent = true;
            TextContent = "";
            FormFields = new List<FormField>();
        }

        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime DueDate { get; set; }
        public FormType Type { get; set; }
        public int Order { get; set; }
        public bool IsCurrent { get; set; }
        public bool Mandatory { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string TextContent { get; set; }

        [Required]
        public Guid FormGroupId { get; set; }
        [Column("FormGroupId")]
        public virtual FormGroup FormGroup { get; set; }

        public Guid? EmailId { get; set; }
        [Column("EmailId")]
        public virtual Email Email { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }

        public virtual ICollection<FormExhibitor> FormExhibitors { get; set; }
        public virtual ICollection<FormField> FormFields { get; set; }
    }
}
