﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("Email")]
    public class Email
    {
        public Email()
        {
            Id = Guid.NewGuid();
            Subject = "A New Email - Created on " + DateTime.Now.ToShortDateString();
            Body = "";
            DateCreated = DateTime.Now;
        }

        [Key]
        public Guid Id { get; set; }
        public string Subject { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string Body { get; set; }
        public bool IsInitialEmail { get; set; }
        public DateTime DateCreated { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }

        public virtual ICollection<EmailLog> EmailLogs { get; set; }
        public virtual ICollection<ExhibitorApplicationEmail> ExhibitorApplicationEmails { get; set; }
    }
}
