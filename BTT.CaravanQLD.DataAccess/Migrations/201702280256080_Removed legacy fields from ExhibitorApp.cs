namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedlegacyfieldsfromExhibitorApp : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ExhibitorApplication", "IsShowContact");
            DropColumn("dbo.ExhibitorApplication", "ShowContactTitle");
            DropColumn("dbo.ExhibitorApplication", "ShowContactFirstName");
            DropColumn("dbo.ExhibitorApplication", "ShowContactSurname");
            DropColumn("dbo.ExhibitorApplication", "ShowContactMobile");
            DropColumn("dbo.ExhibitorApplication", "CQMember");
            DropColumn("dbo.ExhibitorApplication", "CQMemberType");
            DropColumn("dbo.ExhibitorApplication", "CQMembershipID");
            DropColumn("dbo.ExhibitorApplication", "InterstateMember");
            DropColumn("dbo.ExhibitorApplication", "InterstateMemberType");
            DropColumn("dbo.ExhibitorApplication", "InterstateAssociation");
            DropColumn("dbo.ExhibitorApplication", "Contributed");
            DropColumn("dbo.ExhibitorApplication", "NotesAndRequests");
            DropColumn("dbo.ExhibitorApplication", "AcceptedTermsAndConditions");
            DropColumn("dbo.ExhibitorApplication", "DateSubmitted");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExhibitorApplication", "DateSubmitted", c => c.DateTime());
            AddColumn("dbo.ExhibitorApplication", "AcceptedTermsAndConditions", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExhibitorApplication", "NotesAndRequests", c => c.String(maxLength: 2055));
            AddColumn("dbo.ExhibitorApplication", "Contributed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExhibitorApplication", "InterstateAssociation", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "InterstateMemberType", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "InterstateMember", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExhibitorApplication", "CQMembershipID", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "CQMemberType", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "CQMember", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExhibitorApplication", "ShowContactMobile", c => c.String(maxLength: 20));
            AddColumn("dbo.ExhibitorApplication", "ShowContactSurname", c => c.String(maxLength: 50));
            AddColumn("dbo.ExhibitorApplication", "ShowContactFirstName", c => c.String(maxLength: 50));
            AddColumn("dbo.ExhibitorApplication", "ShowContactTitle", c => c.String(maxLength: 50));
            AddColumn("dbo.ExhibitorApplication", "IsShowContact", c => c.Boolean(nullable: false));
        }
    }
}
