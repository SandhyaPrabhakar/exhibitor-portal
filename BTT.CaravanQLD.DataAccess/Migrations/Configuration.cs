﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;

namespace BTT.CaravanQLD.DataAccess.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<CaravanQLDContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CaravanQLDContext context)
        {
            //This is for the multiple show upgrade migration
            var show = context.Shows.FirstOrDefault();
            if (show == null)
            {
                show = new Model.Show();
                show.Name = "Main Show";
                context.Shows.Add(show);

                var contentPages = context.ContentPages.ToList();
                foreach (var contentPage in contentPages)
                {
                    contentPage.LinkedShowId = show.Id;
                }

                var emails = context.Emails.ToList();
                foreach (var email in emails)
                {
                    email.LinkedShowId = show.Id;
                }

                var emailLogs = context.EmailLogs.ToList();
                foreach (var emailLog in emailLogs)
                {
                    emailLog.LinkedShowId = show.Id;
                }

                var emailSettings = context.EmailSettings.ToList();
                foreach (var emailSetting in emailSettings)
                {
                    emailSetting.LinkedShowId = show.Id;
                }

                var exhibitorApplications = context.ExhibitorApplications.ToList();
                foreach (var ea in exhibitorApplications)
                {
                    ea.LinkedShowId = show.Id;
                }

                var formGroups = context.FormGroups.ToList();
                foreach (var formGroup in formGroups)
                {
                    formGroup.LinkedShowId = show.Id;
                }

                var forms = context.Forms.ToList();
                foreach (var form in forms)
                {
                    form.LinkedShowId = show.Id;
                }

                try
                {
                    Directory.CreateDirectory(Path.Combine("W:/vhosts/queenslandcaravanshow.com.au/exhibitorportal.queenslandcaravanshow.com.au/Files/", "Exhibitors", show.LinkIdentifier));
                    context.SaveChanges();
                }
                catch (Exception exc)
                {

                }
            }

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
