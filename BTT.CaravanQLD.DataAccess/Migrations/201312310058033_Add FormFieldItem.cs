namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFormFieldItem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FormFieldItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Text = c.String(),
                        Value = c.String(),
                        FormFieldId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FormFields", t => t.FormFieldId, cascadeDelete: true)
                .Index(t => t.FormFieldId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.FormFieldItems", new[] { "FormFieldId" });
            DropForeignKey("dbo.FormFieldItems", "FormFieldId", "dbo.FormFields");
            DropTable("dbo.FormFieldItems");
        }
    }
}
