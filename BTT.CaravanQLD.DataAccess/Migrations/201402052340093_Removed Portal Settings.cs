namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedPortalSettings : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.PortalSetting");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PortalSetting",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ExhibitorType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
