namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTextContenttoForm : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Forms", "TextContent", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Forms", "TextContent");
        }
    }
}
