namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedProductInfoandSiteInfo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SiteInfo", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropForeignKey("dbo.ProductInfo", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropIndex("dbo.SiteInfo", new[] { "ExhibitorApplicationId" });
            DropIndex("dbo.ProductInfo", new[] { "ExhibitorApplicationId" });
            DropTable("dbo.SiteInfo");
            DropTable("dbo.ProductInfo");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductInfo",
                c => new
                    {
                        ExhibitorApplicationId = c.Guid(nullable: false),
                        Products = c.String(maxLength: 2055),
                        CaravanBrand1 = c.String(maxLength: 50),
                        CaravanBrand2 = c.String(maxLength: 50),
                        CaravanBrand3 = c.String(maxLength: 50),
                        CaravanBrand4 = c.String(maxLength: 50),
                        CaravanBrand5 = c.String(maxLength: 50),
                        CaravanBrand6 = c.String(maxLength: 50),
                        CamperTrailerBrand1 = c.String(maxLength: 50),
                        CamperTrailerBrand2 = c.String(maxLength: 50),
                        CamperTrailerBrand3 = c.String(maxLength: 50),
                        CamperTrailerBrand4 = c.String(maxLength: 50),
                        CamperTrailerBrand5 = c.String(maxLength: 50),
                        CamperTrailerBrand6 = c.String(maxLength: 50),
                        MotorhomeBrand1 = c.String(maxLength: 50),
                        MotorhomeBrand2 = c.String(maxLength: 50),
                        MotorhomeBrand3 = c.String(maxLength: 50),
                        MotorhomeBrand4 = c.String(maxLength: 50),
                        FifthwheelerBrand1 = c.String(maxLength: 50),
                        FifthwheelerBrand2 = c.String(maxLength: 50),
                        FifthwheelerBrand3 = c.String(maxLength: 50),
                        FifthwheelerBrand4 = c.String(maxLength: 50),
                        AccessoriesParts = c.Boolean(nullable: false),
                        AdvisoryServices = c.Boolean(nullable: false),
                        AnnexesAwnings = c.Boolean(nullable: false),
                        ArtUnionsFundraising = c.Boolean(nullable: false),
                        CabinsHomes = c.Boolean(nullable: false),
                        CamperTrailers = c.Boolean(nullable: false),
                        CampingEquipmentTents = c.Boolean(nullable: false),
                        CaravansPopTops = c.Boolean(nullable: false),
                        CarportsCaravanCovers = c.Boolean(nullable: false),
                        ClothingHats = c.Boolean(nullable: false),
                        Clubs = c.Boolean(nullable: false),
                        Communication = c.Boolean(nullable: false),
                        FifthWheelers = c.Boolean(nullable: false),
                        Hire = c.Boolean(nullable: false),
                        Insurance = c.Boolean(nullable: false),
                        MarineFishingBoatTrailers = c.Boolean(nullable: false),
                        MotorhomesCampervansConversions = c.Boolean(nullable: false),
                        Publications = c.Boolean(nullable: false),
                        RoofTopCampers = c.Boolean(nullable: false),
                        SlideOnTraytopCampers = c.Boolean(nullable: false),
                        TouringTourismCaravanParks = c.Boolean(nullable: false),
                        TowingEquipment = c.Boolean(nullable: false),
                        VehiclesVehicleAccessories4WDAccessories = c.Boolean(nullable: false),
                        OtherProductsServices = c.Boolean(nullable: false),
                        AccessHeight = c.String(),
                    })
                .PrimaryKey(t => t.ExhibitorApplicationId);
            
            CreateTable(
                "dbo.SiteInfo",
                c => new
                    {
                        ExhibitorApplicationId = c.Guid(nullable: false),
                        StandChoice1 = c.String(maxLength: 50),
                        StandChoice2 = c.String(maxLength: 50),
                        StandChoice3 = c.String(maxLength: 50),
                        Frontage = c.String(),
                        Depth = c.String(),
                        FrontageRequested = c.String(),
                        DepthRequested = c.String(),
                        Indoors = c.Boolean(nullable: false),
                        Outdoors = c.Boolean(nullable: false),
                        ShellScheme = c.Boolean(nullable: false),
                        Amplification = c.Boolean(nullable: false),
                        PayByCreditCard = c.Boolean(nullable: false),
                        PayByCheque = c.Boolean(nullable: false),
                        SecurityDepositAmount = c.String(),
                        SecurityDepositCardType = c.String(),
                        SecurityDepositCardName = c.String(maxLength: 100),
                        SecurityDepositCardNumber = c.String(maxLength: 19),
                        SecurityDepositCardExpiry = c.String(maxLength: 10),
                        SecurityDepositCCV = c.String(maxLength: 10),
                        BSB = c.String(maxLength: 10),
                        AccountNo = c.String(maxLength: 20),
                        AccountName = c.String(maxLength: 50),
                        BankName = c.String(maxLength: 50),
                        BranchName = c.String(maxLength: 50),
                        AllocatedStandNumber = c.String(),
                    })
                .PrimaryKey(t => t.ExhibitorApplicationId);
            
            CreateIndex("dbo.ProductInfo", "ExhibitorApplicationId");
            CreateIndex("dbo.SiteInfo", "ExhibitorApplicationId");
            AddForeignKey("dbo.ProductInfo", "ExhibitorApplicationId", "dbo.ExhibitorApplication", "Id");
            AddForeignKey("dbo.SiteInfo", "ExhibitorApplicationId", "dbo.ExhibitorApplication", "Id");
        }
    }
}
