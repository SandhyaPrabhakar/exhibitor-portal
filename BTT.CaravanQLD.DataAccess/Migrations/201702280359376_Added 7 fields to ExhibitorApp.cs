namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added7fieldstoExhibitorApp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExhibitorApplication", "SiteLocation", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "Frontage", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "Depth", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "Area", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "MoveInDate", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "AccessGate", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "Notes", c => c.String());

            Sql("update dbo.ExhibitorApplication set SiteLocation = ''");
            Sql("update dbo.ExhibitorApplication set Frontage = ''");
            Sql("update dbo.ExhibitorApplication set Depth = ''");
            Sql("update dbo.ExhibitorApplication set Area = ''");
            Sql("update dbo.ExhibitorApplication set MoveInDate = ''");
            Sql("update dbo.ExhibitorApplication set AccessGate = ''");
            Sql("update dbo.ExhibitorApplication set Notes = ''");
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExhibitorApplication", "Notes");
            DropColumn("dbo.ExhibitorApplication", "AccessGate");
            DropColumn("dbo.ExhibitorApplication", "MoveInDate");
            DropColumn("dbo.ExhibitorApplication", "Area");
            DropColumn("dbo.ExhibitorApplication", "Depth");
            DropColumn("dbo.ExhibitorApplication", "Frontage");
            DropColumn("dbo.ExhibitorApplication", "SiteLocation");
        }
    }
}
