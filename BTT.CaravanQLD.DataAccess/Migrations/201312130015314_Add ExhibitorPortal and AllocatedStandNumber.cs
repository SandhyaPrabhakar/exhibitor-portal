namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExhibitorPortalandAllocatedStandNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExhibitorApplication", "ExhibitorPortal", c => c.Boolean(nullable: false));
            AddColumn("dbo.SiteInfo", "AllocatedStandNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteInfo", "AllocatedStandNumber");
            DropColumn("dbo.ExhibitorApplication", "ExhibitorPortal");
        }
    }
}
