namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAllocatedStandNumbertoExhibitorApplication : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExhibitorApplication", "AllocatedStandNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExhibitorApplication", "AllocatedStandNumber");
        }
    }
}
