// <auto-generated />
namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddEmailandEmailLog : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddEmailandEmailLog));
        
        string IMigrationMetadata.Id
        {
            get { return "201312090629113_Add Email and EmailLog"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
