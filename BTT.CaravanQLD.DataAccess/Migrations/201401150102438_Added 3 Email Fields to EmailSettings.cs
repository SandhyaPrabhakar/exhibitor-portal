namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added3EmailFieldstoEmailSettings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailSetting", "AdminEmailAddress", c => c.String());
            AddColumn("dbo.EmailSetting", "ApplicationEmailAddress", c => c.String());
            AddColumn("dbo.EmailSetting", "EmailSender", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailSetting", "EmailSender");
            DropColumn("dbo.EmailSetting", "ApplicationEmailAddress");
            DropColumn("dbo.EmailSetting", "AdminEmailAddress");
        }
    }
}
