namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLogsPerPagetoEmailSettings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailSetting", "LogsPerPage", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailSetting", "LogsPerPage");
        }
    }
}
