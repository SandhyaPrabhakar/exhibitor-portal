namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedExhibitorPortalandApplicationEmail : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ExhibitorApplication", "ExhibitorPortal");
            DropColumn("dbo.EmailSetting", "ApplicationEmailAddress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EmailSetting", "ApplicationEmailAddress", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "ExhibitorPortal", c => c.Boolean(nullable: false));
        }
    }
}
