namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShow : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Shows",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ShowType = c.Int(nullable: false),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        LinkIdentifier = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ExhibitorApplication", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.EmailLog", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.Email", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.Forms", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.FormGroups", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.EmailSetting", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.ContentPages", "LinkedShowId", c => c.Guid());
            AddForeignKey("dbo.ExhibitorApplication", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.EmailLog", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.Email", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.ContentPages", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.EmailSetting", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.FormGroups", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.Forms", "LinkedShowId", "dbo.Shows", "Id");
            CreateIndex("dbo.ExhibitorApplication", "LinkedShowId");
            CreateIndex("dbo.EmailLog", "LinkedShowId");
            CreateIndex("dbo.Email", "LinkedShowId");
            CreateIndex("dbo.ContentPages", "LinkedShowId");
            CreateIndex("dbo.EmailSetting", "LinkedShowId");
            CreateIndex("dbo.FormGroups", "LinkedShowId");
            CreateIndex("dbo.Forms", "LinkedShowId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Forms", new[] { "LinkedShowId" });
            DropIndex("dbo.FormGroups", new[] { "LinkedShowId" });
            DropIndex("dbo.EmailSetting", new[] { "LinkedShowId" });
            DropIndex("dbo.ContentPages", new[] { "LinkedShowId" });
            DropIndex("dbo.Email", new[] { "LinkedShowId" });
            DropIndex("dbo.EmailLog", new[] { "LinkedShowId" });
            DropIndex("dbo.ExhibitorApplication", new[] { "LinkedShowId" });
            DropForeignKey("dbo.Forms", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.FormGroups", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.EmailSetting", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.ContentPages", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.Email", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.EmailLog", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.ExhibitorApplication", "LinkedShowId", "dbo.Shows");
            DropColumn("dbo.ContentPages", "LinkedShowId");
            DropColumn("dbo.EmailSetting", "LinkedShowId");
            DropColumn("dbo.FormGroups", "LinkedShowId");
            DropColumn("dbo.Forms", "LinkedShowId");
            DropColumn("dbo.Email", "LinkedShowId");
            DropColumn("dbo.EmailLog", "LinkedShowId");
            DropColumn("dbo.ExhibitorApplication", "LinkedShowId");
            DropTable("dbo.Shows");
        }
    }
}
