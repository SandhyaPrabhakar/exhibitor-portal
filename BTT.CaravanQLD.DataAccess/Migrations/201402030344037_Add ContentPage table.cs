namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddContentPagetable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContentPages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PageType = c.Int(nullable: false),
                        Name = c.String(),
                        TextContent = c.String(unicode: false),
                        Order = c.Int(nullable: false),
                        IsCurrent = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ContentPages");
        }
    }
}
