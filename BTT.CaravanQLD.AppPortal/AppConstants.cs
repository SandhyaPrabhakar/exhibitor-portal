﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal
{
    public class AppConstants
    {
        public const string AdminUserName = "portaladmin";
        public const string AdminPassword = "caravanqld1";
        public const string FieldPlaceholderText = "0";

        public const string SmptHostName = "mail.docsontap.com.au";
        public const string SmptUsername = "userkeys@docsontap.com.au";
        public const string SmptPassword = "docs66sys!";
        //public const string EmailFromAddress = "info@caravanqld.com.au";
        //public const string EmailFromName = "Caravanning Queensland";

        public const string RequiredErrorMessage = "{0} is a required field.";
        public const string TextErrorMessage = "{0} is a required field and must be less than {1} characters.";
        public const string LessThanMaxErrorMessage = "{0} must be less than {1} characters.";
        public const string RequiredLengthOnlyErrorMessage = "{0} must be greater than {1} and less than {2} characters.";
        public const string ValidPhoneNumberErrorMessage = "{0} is not a valid phone number. You must include the area code for local numbers (and for mobiles in NZ).";
        public const string ValidEmailAddressErrorMessage = "{0} is not a valid email address.";
        public const string CompareEmailsErrorMessage = "{0} confirmation is not the same as {0}.";
        public const string UrlErrorMessage = "{0} is not a valid URL.";
        public const string MembershipTypeErrorMessage = "You must pick one of the membership types.";
        public const string DecimalErrorMEssage = "{0} must be left blank or contain a decimal number only.";

        //http://regexlib.com/REDetails.aspx?regexp_id=2054
        public const string AustralianPhoneRegex = @"^(\+\d{2}[ \-]{0,1}){0,1}(((\({0,1}[ \-]{0,1})0{0,1}\){0,1}[2|3|7|8]{1}\){0,1}[ \-]*(\d{4}[ \-]{0,1}\d{4}))|(1[ \-]{0,1}(300|800|900|902)[ \-]{0,1}((\d{6})|(\d{3}[ \-]{0,1}\d{3})))|(13[ \-]{0,1}([\d \-]{5})|((\({0,1}[ \-]{0,1})0{0,1}\){0,1}4{1}[\d \-]{8,10})))$";
        //http://regexlib.com/REDetails.aspx?regexp_id=1600
        public const string NZPhoneRegex = @"(^\([0]\d{1}\))(\d{7}$)|(^\([0][2]\d{1}\))(\d{6,8}$)|([0][8][0][0])([\s])(\d{5,8}$)";
        public const string EmailRegex = @"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$";
        public const string UrlRegex = @"^((http|HTTP|https|HTTPS|ftp|FTP?)\:\/\/)?((www|WWW)+\.)+(([0-9]{1,3}){3}[0-9]{1,3}\.|([\w!~*'()-]+\.)*([\w^-][\w-]{0,61})?[\w]\.[a-z]{2,6})(:[0-9]{1,4})?((\/*)|(\/+[\w!~*'().;?:@&=+$,%#-]+)+\/*)$";

        public const string CurrentShowAdminSession = "CurrentShowAdmin";
        public const string CurrentShowSession = "CurrentShowSession";
        public const string FilesBaseDir = "W:/vhosts/queenslandcaravanshow.com.au/exhibitorportal.queenslandcaravanshow.com.au/Files/";
    }
}