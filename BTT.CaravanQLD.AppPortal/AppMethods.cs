﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal
{
    public class AppMethods
    {
        public static List<SelectListItem> CreateListOfStates()
        {
            var stateList = new List<SelectListItem>();
            stateList.Add(new SelectListItem() { Text = "", Value = "" });
            stateList.Add(new SelectListItem() { Text = "ACT - Australian Capital Territory", Value = "ACT" });
            stateList.Add(new SelectListItem() { Text = "NSW - New South Wales", Value = "NSW" });
            stateList.Add(new SelectListItem() { Text = "NT - Northern Territory", Value = "NT" });
            stateList.Add(new SelectListItem() { Text = "NZ - New Zealand", Value = "NZ" });
            stateList.Add(new SelectListItem() { Text = "QLD - Queensland", Value = "QLD" });
            stateList.Add(new SelectListItem() { Text = "SA - South Australia", Value = "SA" });
            stateList.Add(new SelectListItem() { Text = "TAS - Tasmania", Value = "TAS" });
            stateList.Add(new SelectListItem() { Text = "VIC - Victoria", Value = "VIC" });
            stateList.Add(new SelectListItem() { Text = "WA - Western Australia", Value = "WA" });
            return stateList;
        }

        public static List<SelectListItem> CreateListOfAssociations()
        {
            var assoList = new List<SelectListItem>();
            assoList.Add(new SelectListItem() { Text = "", Value = "" });
            assoList.Add(new SelectListItem() { Text = "Caravan Parks Qld", Value = "Caravan Parks Qld" });
            assoList.Add(new SelectListItem() { Text = "CCIA of NSW", Value = "CCIA of NSW" });
            assoList.Add(new SelectListItem() { Text = "CIA Victoria", Value = "CIA Victoria" });
            assoList.Add(new SelectListItem() { Text = "Caravan Industry WA", Value = "Caravan Industry WA" });
            assoList.Add(new SelectListItem() { Text = "CCIA of SA", Value = "CCIA of SA" });
            assoList.Add(new SelectListItem() { Text = "Vic Parks", Value = "Vic Parks" });
            assoList.Add(new SelectListItem() { Text = "Caravan Parks of SA", Value = "Caravan Parks of SA" });
            assoList.Add(new SelectListItem() { Text = "Caravan Industry Tas", Value = "Caravan Industry Tas" });
            assoList.Add(new SelectListItem() { Text = "CPA NT", Value = "CPA NT" });
            assoList.Add(new SelectListItem() { Text = "RVM Australia", Value = "RVM Australia" });
            return assoList;
        }

        public static string ReplaceSmartFields(ExhibitorApplication app, string text, EmailSetting emailSettings)
        {
            text = text.Replace("%CurrentShowTitle%", String.IsNullOrEmpty(emailSettings.ShowTitle) ? "" : emailSettings.ShowTitle);

            text = text.Replace("%ShowId%", String.IsNullOrEmpty(app.ShowId) ? "" : app.ShowId);
            text = text.Replace("%TradingName%", String.IsNullOrEmpty(app.TradingName) ? "" : app.TradingName);
            text = text.Replace("%ShowListingName%", String.IsNullOrEmpty(app.ShowListingName) ? "" : app.ShowListingName);
            text = text.Replace("%AllocatedStandNumber%", String.IsNullOrEmpty(app.AllocatedStandNumber) ? "" : app.AllocatedStandNumber);
            text = text.Replace("%SiteLocation%", String.IsNullOrEmpty(app.SiteLocation) ? "" : app.SiteLocation);
            text = text.Replace("%Frontage%", String.IsNullOrEmpty(app.Frontage) ? "" : app.Frontage);
            text = text.Replace("%Depth%", String.IsNullOrEmpty(app.Depth) ? "" : app.Depth);
            text = text.Replace("%Area%", String.IsNullOrEmpty(app.Area) ? "" : app.Area);
            text = text.Replace("%AddressLine1%", String.IsNullOrEmpty(app.AddressLine1) ? "" : app.AddressLine1);
            text = text.Replace("%AddressLine2%", String.IsNullOrEmpty(app.AddressLine2) ? "" : app.AddressLine2);
            text = text.Replace("%Suburb%", String.IsNullOrEmpty(app.Suburb) ? "" : app.Suburb);
            text = text.Replace("%State%", String.IsNullOrEmpty(app.State) ? "" : app.State);
            text = text.Replace("%Country%", String.IsNullOrEmpty(app.Country) ? "" : app.Country);
            text = text.Replace("%Postcode%", String.IsNullOrEmpty(app.Postcode) ? "" : app.Postcode);
            text = text.Replace("%ExhibitorPhone%", String.IsNullOrEmpty(app.ExhibitorPhone) ? "" : app.ExhibitorPhone);
            text = text.Replace("%ExhibitorFax%", String.IsNullOrEmpty(app.ExhibitorFax) ? "" : app.ExhibitorFax);
            text = text.Replace("%ExhibitorEmail%", String.IsNullOrEmpty(app.ExhibitorEmail) ? "" : app.ExhibitorEmail);

            text = text.Replace("%Title%", String.IsNullOrEmpty(app.Title) ? "" : app.Title);
            text = text.Replace("%FirstName%", String.IsNullOrEmpty(app.FirstName) ? "" : app.FirstName);
            text = text.Replace("%Surname%", String.IsNullOrEmpty(app.Surname) ? "" : app.Surname);
            text = text.Replace("%Mobile%", String.IsNullOrEmpty(app.Mobile) ? "" : app.Mobile);
            text = text.Replace("%Phone%", String.IsNullOrEmpty(app.Phone) ? "" : app.Phone);
            text = text.Replace("%Email%", String.IsNullOrEmpty(app.Email) ? "" : app.Email);
            text = text.Replace("%MoveInDate%", String.IsNullOrEmpty(app.MoveInDate) ? "" : app.MoveInDate);
            text = text.Replace("%AccessTime%", String.IsNullOrEmpty(app.AccessTime) ? "" : app.AccessTime);
            text = text.Replace("%AccessGate%", String.IsNullOrEmpty(app.AccessGate) ? "" : app.AccessGate);
            text = text.Replace("%Notes%", String.IsNullOrEmpty(app.Notes) ? "" : app.Notes);

            return text;
        }

        public static string ConvertBoolToYesNo(bool value)
        {
            if (value)
                return "Yes";
            else
                return "No";
        }

        public static bool IsTextValid(string value, int maxLength)
        {
            if (!String.IsNullOrEmpty(value) && (maxLength == 0 || value.Length <= maxLength))
                return true;
            else
                return false;
        }

        public static bool IsLessThanMax(string value, int maxLength)
        {
            if (String.IsNullOrEmpty(value) || (!String.IsNullOrEmpty(value) && (maxLength == 0 || value.Length <= maxLength)))
                return true;
            else
                return false;
        }

        public static bool IsRequiredLengthOnly(string value, int minLength, int maxLength)
        {
            if (String.IsNullOrEmpty(value) || (!String.IsNullOrEmpty(value) && value.Length >= minLength && (maxLength == 0 || value.Length <= maxLength)))
                return true;
            else
                return false;
        }

        public static bool IsPhoneNumber(string value)
        {
            if (!String.IsNullOrEmpty(value) && (Regex.IsMatch(value, AppConstants.AustralianPhoneRegex) || Regex.IsMatch(value, AppConstants.NZPhoneRegex)))
                return true;
            else
                return false;
        }

        public static bool IsEmailAddress(string value)
        {
            if (!String.IsNullOrEmpty(value) && Regex.IsMatch(value, AppConstants.EmailRegex))
                return true;
            else
                return false;
        }

        public static bool IsUrl(string value)
        {
            if (!String.IsNullOrEmpty(value) && Regex.IsMatch(value, AppConstants.UrlRegex))
                return true;
            else
                return false;
        }

        public static bool IsSameEmail(string value, string valueToCompare)
        {
            if (!String.IsNullOrEmpty(value) && !String.IsNullOrEmpty(valueToCompare) && value == valueToCompare)
                return true;
            else
                return false;
        }

        public static bool IsBrandValid(string value)
        {
            if (String.IsNullOrEmpty(value) || (!String.IsNullOrEmpty(value) && value.Length <= 50))
                return true;
            else
                return false;
        }
    }
}