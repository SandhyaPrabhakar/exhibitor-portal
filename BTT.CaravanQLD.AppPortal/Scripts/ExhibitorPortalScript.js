﻿/* Exhibitor Portal Functions */
$(document).ready(function () {
    $.ajaxSetup({
        cache: false
    });
});

function openFileUploaderForExhibitor(showId, formFieldId, currentShowLink) {
    var finder = new CKFinder();
    finder.basePath = '../../Scripts/ckfinder-ex/';
    finder.startupPath = 'Exhibitors:/' + currentShowLink + '/' + showId + '/';
    finder.selectActionData = formFieldId;
    finder.selectActionFunction = function (fileUrl, data) {
        var filename = fileUrl.replace(/^.*[\\\/]/, '').replace(/%20/g, ' ');
        var formFieldId = data['selectActionData'];
        $("div#" + formFieldId).html(filename);
        $("input[name='" + formFieldId + "']").val(filename);
    };
    finder.popup();
}

function scrollToTop() {
    $(window).scrollTop(0);
    //if ($(".field-validation-error")[0] != undefined) {
    //    $("#validation-overview").css("display", "block");
    //}
}

function hookUpForm() {
    $('form')[0].onkeypress = checkForEnter;
}

function checkForEnter(e) {
    e = e || event;
    var txtArea = /textarea/i.test((e.target || e.srcElement).tagName);
    return txtArea || (e.keyCode || e.which || e.charCode || 0) !== 13;
}