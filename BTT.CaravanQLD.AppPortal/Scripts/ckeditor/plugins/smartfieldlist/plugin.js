﻿//This is adapted from here:http://tizardsbriefcase.com/908/javascript/custom-dropdown-in-ckeditor-4

CKEDITOR.plugins.add('smartfieldlist',
{
    requires: ['richcombo'], //, 'styles' ],
    init: function (editor) {
        var config = editor.config,
           lang = editor.lang.format;

        // Create the list of smart fields
        var tags = []; //new Array();
        tags.push(["%ShowId%", "Exhibitor - Show ID", "Exhibitor - Show ID"]);
        tags.push(["%TradingName%", "Exhibitor - Trading Name", "Exhibitor - Trading Name"]);
        tags.push(["%ShowListingName%", "Exhibitor - Show Listing Name", "Exhibitor - Show Listing Name"]);
        tags.push(["%AllocatedStandNumber%", "Site - Allocated Stand Number", "Site - Allocated Stand Number"]);
        tags.push(["%SiteLocation%", "Site - Location", "Site - Location"]);
        tags.push(["%Frontage%", "Site - Frontage", "Site - Frontage"]);
        tags.push(["%Depth%", "Site - Depth", "Site - Depth"]);
        tags.push(["%Area%", "Site - Area", "Site - Area"]);
        tags.push(["%AddressLine1%", "Exhibitor - Address Line 1", "Exhibitor - Address Line 1"]);
        tags.push(["%AddressLine2%", "Exhibitor - Address Line 2", "Exhibitor - Address Line 2"]);
        tags.push(["%Suburb%", "Exhibitor - Suburb", "Exhibitor - Suburb"]);
        tags.push(["%State%", "Exhibitor - State", "Exhibitor - State"]);
        tags.push(["%Country%", "Exhibitor - Country", "Exhibitor - Country"]);
        tags.push(["%Postcode%", "Exhibitor - Postcode", "Exhibitor - Postcode"]);
        tags.push(["%ExhibitorPhone%", "Exhibitor - Exhibitor Phone", "Exhibitor - Exhibitor Phone"]);
        tags.push(["%ExhibitorFax%", "Exhibitor - Exhibitor Fax", "Exhibitor - Exhibitor Fax"]);
        tags.push(["%ExhibitorEmail%", "Exhibitor - Exhibitor Email", "Exhibitor - Exhibitor Email"]);
        tags.push(["%Title%", "Contact - Title", "Contact - Title"]);
        tags.push(["%FirstName%", "Contact - First Name", "Contact - First Name"]);
        tags.push(["%Surname%", "Contact - Surname", "Contact - Surname"]);
        tags.push(["%Mobile%", "Contact - Mobile", "Contact - Mobile"]);
        tags.push(["%Phone%", "Contact - Phone", "Contact - Phone"]);
        tags.push(["%Email%", "Contact - Email", "Contact - Email"]);
        tags.push(["%MoveInDate%", "Misc - Move In Date", "Misc - Move In Date"]);
        tags.push(["%AccessTime%", "Misc - Access Time", "Misc - Access Time"]);
        tags.push(["%AccessGate%", "Misc - Access Gate", "Misc - Access Gate"]);
        tags.push(["%Notes%", "Misc - Notes", "Misc - Notes"]);
        //tags.push(["%IsShowContact%", "Contact - Is Show Contact", "Contact - Is Show Contact"]);
        //tags.push(["%ShowContactTitle%", "Contact - Show Contact Title", "Contact - Show Contact Title"]);
        //tags.push(["%ShowContactFirstName%", "Contact - Show Contact First Name", "Contact - Show Contact First Name"]);
        //tags.push(["%ShowContactSurname%", "Contact - Show Contact Surname", "Contact - Show Contact Surname"]);
        //tags.push(["%ShowContactMobile%", "Contact - Show Contact Mobile", "Contact - Show Contact Mobile"]);

        //tags.push(["%CQMember%", "Member / CRVA - CQ Member", "Member / CRVA - CQ Member"]);
        //tags.push(["%CQMemberType%", "Member / CRVA - CQ Member Type", "Member / CRVA - CQ Member Type"]);
        //tags.push(["%CQMembershipID%", "Member / CRVA - CQ Membership ID", "Member / CRVA - CQ Membership ID"]);
        //tags.push(["%InterstateMember%", "Member / CRVA - Interstate Member", "Member / CRVA - Interstate Member"]);
        //tags.push(["%InterstateMemberType%", "Member / CRVA - Interstate Member Type", "Member / CRVA - Interstate Member Type"]);
        //tags.push(["%InterstateAssociation%", "Member / CRVA - Interstate Association", "Member / CRVA - Interstate Association"]);
        //tags.push(["%Contributed%", "Member / CRVA - Contributed", "Member / CRVA - Contributed"]);

        //tags.push(["%NotesAndRequests%", "Misc - Notes And Requests", "Misc - Notes And Requests"]);
        //tags.push(["%AcceptedTermsAndConditions%", "Misc - Accepted Terms And Conditions", "Misc - Accepted Terms And Conditions"]);
        //tags.push(["%DateSubmitted%", "Misc - Date Submitted", "Misc - Date Submitted"]);
        //tags.push(["%ExhibitorPortal%", "Misc - Exhibitor Portal", "Misc - Exhibitor Portal"]);

        //tags.push(["%StandChoice1%", "Site - Stand Choice 1", "Site - Stand Choice 1"]);
        //tags.push(["%StandChoice2%", "Site - Stand Choice 2", "Site - Stand Choice 2"]);
        //tags.push(["%StandChoice3%", "Site - Stand Choice 3", "Site - Stand Choice 3"]);
        //tags.push(["%Frontage%", "Site - Frontage", "Site - Frontage"]);
        //tags.push(["%Depth%", "Site - Depth", "Site - Depth"]);
        //tags.push(["%FrontageRequested%", "Site - Frontage Requested", "Site - Frontage Requested"]);
        //tags.push(["%DepthRequested%", "Site - Depth Requested", "Site - Depth Requested"]);
        //tags.push(["%Indoors%", "Site - Indoors", "Site - Indoors"]);
        //tags.push(["%Outdoors%", "Site - Outdoors", "Site - Outdoors"]);
        //tags.push(["%ShellScheme%", "Site - Shell Scheme", "Site - Shell Scheme"]);
        //tags.push(["%Amplification%", "Site - Amplification", "Site - Amplification"]);
        //tags.push(["%AccessHeight%", "Site - Access Height", "Site - Access Height"]);
        //tags.push(["%AllocatedStandNumber%", "Site - Allocated Stand Number", "Site - Allocated Stand Number"]);

        //tags.push(["%Products%", "Products - Products", "Products - Products"]);
        //tags.push(["%CaravanBrand1%", "Products - Caravan Brand 1", "Products - Caravan Brand 1"]);
        //tags.push(["%CaravanBrand2%", "Products - Caravan Brand 2", "Products - Caravan Brand 2"]);
        //tags.push(["%CaravanBrand3%", "Products - Caravan Brand 3", "Products - Caravan Brand 3"]);
        //tags.push(["%CaravanBrand4%", "Products - Caravan Brand 4", "Products - Caravan Brand 4"]);
        //tags.push(["%CaravanBrand5%", "Products - Caravan Brand 5", "Products - Caravan Brand 5"]);
        //tags.push(["%CaravanBrand6%", "Products - Caravan Brand 6", "Products - Caravan Brand 6"]);
        //tags.push(["%CamperTrailerBrand1%", "Products - Camper Trailer Brand 1", "Products - Camper Trailer Brand 1"]);
        //tags.push(["%CamperTrailerBrand2%", "Products - Camper Trailer Brand 2", "Products - Camper Trailer Brand 2"]);
        //tags.push(["%CamperTrailerBrand3%", "Products - Camper Trailer Brand 3", "Products - Camper Trailer Brand 3"]);
        //tags.push(["%CamperTrailerBrand4%", "Products - Camper Trailer Brand 4", "Products - Camper Trailer Brand 4"]);
        //tags.push(["%CamperTrailerBrand5%", "Products - Camper Trailer Brand 5", "Products - Camper Trailer Brand 5"]);
        //tags.push(["%CamperTrailerBrand6%", "Products - Camper Trailer Brand 6", "Products - Camper Trailer Brand 6"]);
        //tags.push(["%MotorhomeBrand1%", "Products - Motorhome Brand 1", "Products - Motorhome Brand 1"]);
        //tags.push(["%MotorhomeBrand2%", "Products - Motorhome Brand 2", "Products - Motorhome Brand 2"]);
        //tags.push(["%MotorhomeBrand3%", "Products - Motorhome Brand 3", "Products - Motorhome Brand 3"]);
        //tags.push(["%MotorhomeBrand4%", "Products - Motorhome Brand 4", "Products - Motorhome Brand 4"]);
        //tags.push(["%FifthwheelerBrand1%", "Products - Fifthwheeler Brand 1", "Products - Fifthwheeler Brand 1"]);
        //tags.push(["%FifthwheelerBrand2%", "Products - Fifthwheeler Brand 2", "Products - Fifthwheeler Brand 2"]);
        //tags.push(["%FifthwheelerBrand3%", "Products - Fifthwheeler Brand 3", "Products - Fifthwheeler Brand 3"]);
        //tags.push(["%FifthwheelerBrand4%", "Products - Fifthwheeler Brand 4", "Products - Fifthwheeler Brand 4"]);
        //tags.push(["%AccessoriesParts%", "Products - Accessories Parts", "Products - Accessories Parts"]);
        //tags.push(["%AdvisoryServices%", "Products - Advisory Services", "Products - Advisory Services"]);
        //tags.push(["%AnnexesAwnings%", "Products - Annexes Awnings", "Products - Annexes Awnings"]);
        //tags.push(["%ArtUnionsFundraising%", "Products - Art Unions Fundraising", "Products - Art Unions Fundraising"]);
        //tags.push(["%CabinsHomes%", "Products - Cabins Homes", "Products - Cabins Homes"]);
        //tags.push(["%CamperTrailers%", "Products - Camper Trailers", "Products - Camper Trailers"]);
        //tags.push(["%CampingEquipmentTents%", "Products - Camping Equipment Tents", "Products - Camping Equipment Tents"]);
        //tags.push(["%CaravansPopTops%", "Products - Caravans Pop Tops", "Products - Caravans Pop Tops"]);
        //tags.push(["%CarportsCaravanCovers%", "Products - Carports Caravan Covers", "Products - Carports Caravan Covers"]);
        //tags.push(["%ClothingHats%", "Products - Clothing Hats", "Products - Clothing Hats"]);
        //tags.push(["%Clubs%", "Products - Clubs", "Products - Clubs"]);
        //tags.push(["%Communication%", "Products - Communication", "Products - Communication"]);
        //tags.push(["%FifthWheelers%", "Products - Fifthwheelers", "Products - Fifthwheelers"]);
        //tags.push(["%Hire%", "Products - Hire", "Products - Hire"]);
        //tags.push(["%Insurance%", "Products - Insurance", "Products - Insurance"]);
        //tags.push(["%MarineFishingBoatTrailers%", "Products - Marine Fishing Boat Trailers", "Products - Marine Fishing Boat Trailers"]);
        //tags.push(["%MotorhomesCampervansConversions%", "Products - Motorhomes Campervans Conversions", "Products - Motorhomes Campervans Conversions"]);
        //tags.push(["%Publications%", "Products - Publications", "Products - Publications"]);
        //tags.push(["%RoofTopCampers%", "Products - Roof Top Campers", "Products - Roof Top Campers"]);
        //tags.push(["%SlideOnTraytopCampers%", "Products - Slide On Traytop Campers", "Products - Slide On Traytop Campers"]);
        //tags.push(["%TouringTourismCaravanParks%", "Products - Touring Tourism Caravan Parks", "Products - Touring Tourism Caravan Parks"]);
        //tags.push(["%TowingEquipment%", "Products - Towing Equipment", "Products - Towing Equipment"]);
        //tags.push(["%VehiclesVehicleAccessories4WDAccessories%", "Products - Vehicles Vehicle Accessories 4WD Accessories", "Products - Vehicles Vehicle Accessories 4WD Accessories"]);
        //tags.push(["%OtherProductsServices%", "Products - Other Products Services", "Products - Other Products Services"]);
        tags.sort(function (a, b) {
            if (a[1] < b[1]) { return -1 };
            if (a[1] > b[1]) { return 1 };
            return 0;
        });;
        tags.splice(0, 0, ["%CurrentShowTitle%", "Current Show Title", "Current Show Title"]);

        // Create style objects for all defined styles.

        editor.ui.addRichCombo('smartfieldlist',
           {
               label: "Smart Fields",
               title: "Insert Smart Fields",
               voiceLabel: "Insert Smart Fields",
               className: 'cke_format',
               multiSelect: false,

               panel:
               {
                   css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
                   voiceLabel: editor.lang.panelVoiceLabel
               },

               init: function () {
                   this.startGroup("Smart Fields");
                   //this.add('value', 'drop_text', 'drop_label');
                   for (var this_tag in tags) {
                       this.add(tags[this_tag][0], tags[this_tag][1], tags[this_tag][2]);
                   }
               },

               onClick: function (value) {
                   editor.focus();
                   editor.fire('saveSnapshot');
                   editor.insertHtml(value);
                   editor.fire('saveSnapshot');
               }
           });
    }
});
