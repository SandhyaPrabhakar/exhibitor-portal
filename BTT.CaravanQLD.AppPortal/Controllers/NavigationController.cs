﻿using BTT.CaravanQLD.AppPortal.Models;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    public class NavigationController : Controller
    {
        private Show GetCurrentShow()
        {
            var showId = (Guid)Session[AppConstants.CurrentShowSession];
            if (showId != null)
            {
                using (var db = new CaravanQLDContext())
                {
                    var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                    return show;
                }
            }
            else
            {
                return null;
            }
        }

        public ActionResult Index()
        {
            using (var db = new CaravanQLDContext())
            {
                var model = new NavigationModel();
                if (!String.IsNullOrEmpty(User.Identity.Name))
                {
                    var currentShow = GetCurrentShow();
                    var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                    var pages = db.ContentPages.Where(x => x.IsCurrent && x.PageType == PageType.Custom && x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();

                    model = new NavigationModel()
                    {
                        Pages = pages,
                        Exhibitor = exhibitor
                    };
                }
                else
                {
                    model.Pages = new List<ContentPage>();
                }

                return View(model);
            }
        }

        [Authorize]
        public ActionResult Page(string page)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var pageName = page.Replace("_", " ").Replace("amp1", "&");
                var contentPage = db.ContentPages.FirstOrDefault(x => x.Name == pageName && x.LinkedShowId == currentShow.Id);

                var model = new PageModel()
                {
                    ContentPage = contentPage
                };

                return PartialView(model);
            }
        }

    }
}
