﻿using BTT.CaravanQLD.AppPortal.Models;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    public class Exhibitor_ProfileController : Controller
    {
        private Show GetCurrentShow()
        {
            var showId = (Guid)Session[AppConstants.CurrentShowSession];
            if (showId != null)
            {
                using (var db = new CaravanQLDContext())
                {
                    var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                    return show;
                }
            }
            else
            {
                return null;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                var model = new EditProfileModel()
                {
                    Exhibitor = exhibitor,
                    StateList = AppMethods.CreateListOfStates(),
                    EmailSettings = emailSettings
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(EditProfileModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                if (!ModelState.IsValid)
                {
                    ModelState.Clear();
                }

                ValidateExhibitorDetails(model);
                var currentShow = GetCurrentShow();

                if (ModelState.IsValid)
                {
                    var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == model.Exhibitor.Id);
                    var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    var updateEmailBody = "";
                    //The following exhibitor: Exhibitor Name: Lavender Composites Exhibitor ID: CQS0000 has changed their Exhibitor Email to: doug.m@lavender-CE.com

                    if (exhibitor.Title != model.Exhibitor.Title)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Contact Title/Position", model.Exhibitor.Title, Environment.NewLine);
                    exhibitor.Title = model.Exhibitor.Title;

                    if (exhibitor.FirstName != model.Exhibitor.FirstName)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Contact First Name", model.Exhibitor.FirstName, Environment.NewLine);
                    exhibitor.FirstName = model.Exhibitor.FirstName;

                    if (exhibitor.Surname != model.Exhibitor.Surname)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Contact Surname", model.Exhibitor.Surname, Environment.NewLine);
                    exhibitor.Surname = model.Exhibitor.Surname;

                    if (exhibitor.Mobile != model.Exhibitor.Mobile)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Contact Mobile", model.Exhibitor.Mobile, Environment.NewLine);
                    exhibitor.Mobile = model.Exhibitor.Mobile;

                    if (exhibitor.Phone != model.Exhibitor.Phone)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Contact Phone", model.Exhibitor.Phone, Environment.NewLine);
                    exhibitor.Phone = model.Exhibitor.Phone;

                    if (exhibitor.Email != model.Exhibitor.Email)
                        updateEmailBody += String.Format("{0} to: {1}", "Contact Email", model.Exhibitor.Email, Environment.NewLine);
                    exhibitor.Email = model.Exhibitor.Email;

                    if (exhibitor.AddressLine1 != model.Exhibitor.AddressLine1)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Address Line 1", model.Exhibitor.AddressLine1, Environment.NewLine);
                    exhibitor.AddressLine1 = model.Exhibitor.AddressLine1;

                    if (!String.IsNullOrEmpty(model.Exhibitor.AddressLine2) && exhibitor.AddressLine2 != model.Exhibitor.AddressLine2)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Address Line 2", model.Exhibitor.AddressLine2, Environment.NewLine);
                    exhibitor.AddressLine2 = String.IsNullOrEmpty(model.Exhibitor.AddressLine2) ? "" : model.Exhibitor.AddressLine2;

                    if (exhibitor.Suburb != model.Exhibitor.Suburb)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Suburb", model.Exhibitor.Suburb, Environment.NewLine);
                    exhibitor.Suburb = model.Exhibitor.Suburb;

                    if (exhibitor.State != model.Exhibitor.State)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "State", model.Exhibitor.State, Environment.NewLine);
                    exhibitor.State = model.Exhibitor.State;

                    if (exhibitor.Country != model.Exhibitor.Country)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Country", model.Exhibitor.Country, Environment.NewLine);
                    exhibitor.Country = model.Exhibitor.Country;

                    if (exhibitor.Postcode != model.Exhibitor.Postcode)
                        updateEmailBody += String.Format("{2}{0} to: {1}", "Postcode", model.Exhibitor.Postcode, Environment.NewLine);
                    exhibitor.Postcode = model.Exhibitor.Postcode;

                    db.SaveChanges();

                    if (!String.IsNullOrEmpty(updateEmailBody))
                    {
                        updateEmailBody = String.Format("The exhibitor with Trading Name: {0} and Show ID: {1} has updated their details.", exhibitor.TradingName, exhibitor.ShowId) + updateEmailBody;

                        //Send Update Email
                        #if Debug
                            SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
                        #else
                            SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                            client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
                        #endif

                        MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                        MailAddress to = new MailAddress(emailSettings.AdminEmailAddress);
                        MailMessage message = new MailMessage(from, to);
                        message.Body = updateEmailBody;
                        message.IsBodyHtml = true;
                        message.Subject = "Exhibitor Details Update - Caravanning Queensland";
                        client.Send(message);
                    }

                    model.StateList = AppMethods.CreateListOfStates();
                    model.EmailSettings = emailSettings;
                    return PartialView(model);
                }

                model.StateList = AppMethods.CreateListOfStates();
                model.EmailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                return PartialView(model);
            }
        }

        private void ValidateExhibitorDetails(EditProfileModel model)
        {
            var exhibitor = model.Exhibitor;
            if (!AppMethods.IsTextValid(exhibitor.Title, 50))
                ModelState.AddModelError("Exhibitor.Title", String.Format(AppConstants.TextErrorMessage, "Contact Title / Position", "50"));

            if (!AppMethods.IsTextValid(exhibitor.FirstName, 50))
                ModelState.AddModelError("Exhibitor.FirstName", String.Format(AppConstants.TextErrorMessage, "Contact First Name", "50"));

            if (!AppMethods.IsTextValid(exhibitor.Surname, 50))
                ModelState.AddModelError("Exhibitor.Surname", String.Format(AppConstants.TextErrorMessage, "Contact Surname", "50"));

            if (!AppMethods.IsPhoneNumber(exhibitor.Mobile))
                ModelState.AddModelError("Exhibitor.Mobile", String.Format(AppConstants.ValidPhoneNumberErrorMessage, "Contact Mobile"));

            if (!AppMethods.IsPhoneNumber(exhibitor.Phone))
                ModelState.AddModelError("Exhibitor.Phone", String.Format(AppConstants.ValidPhoneNumberErrorMessage, "Contact Phone"));

            if (!AppMethods.IsEmailAddress(exhibitor.Email))
                ModelState.AddModelError("Exhibitor.Email", String.Format(AppConstants.ValidEmailAddressErrorMessage, "Contact Email"));

            if (!AppMethods.IsTextValid(exhibitor.AddressLine1, 150))
                ModelState.AddModelError("Exhibitor.AddressLine1", String.Format(AppConstants.TextErrorMessage, "Address Line 1", "150"));

            if (!AppMethods.IsLessThanMax(exhibitor.AddressLine2, 150))
                ModelState.AddModelError("Exhibitor.AddressLine2", String.Format(AppConstants.LessThanMaxErrorMessage, "Trading Name", "150"));

            if (!AppMethods.IsTextValid(exhibitor.Suburb, 50))
                ModelState.AddModelError("Exhibitor.Suburb", String.Format(AppConstants.TextErrorMessage, "Suburb / Town", "50"));

            if (!AppMethods.IsTextValid(exhibitor.State, 50))
                ModelState.AddModelError("Exhibitor.State", String.Format(AppConstants.TextErrorMessage, "State", "50"));

            if (!AppMethods.IsTextValid(exhibitor.Country, 50))
                ModelState.AddModelError("Exhibitor.Country", String.Format(AppConstants.TextErrorMessage, "Country", "50"));

            if (!AppMethods.IsTextValid(exhibitor.Postcode, 10))
                ModelState.AddModelError("Exhibitor.Postcode", String.Format(AppConstants.TextErrorMessage, "Postcode", "10"));
        }
    }
}
