﻿using BTT.CaravanQLD.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    [Authorize]
    public class WelcomeController : Controller
    {
        //
        // GET: /Welcome/

        public ActionResult Index()
        {
            if (User.Identity.Name == AppConstants.AdminUserName)
                return RedirectToAction("LogOut", "Account");
            else
            {
                using (var db = new CaravanQLDContext())
                {
                    var firstPage = db.ContentPages.Where(x => x.IsCurrent && x.PageType == PageType.Custom).OrderBy(x => x.Order).FirstOrDefault();
                    if (firstPage != null)
                    {
                        return RedirectToRoute(new { action = "Page", page = firstPage.LinkName });
                    }
                    else
                    {
                        return RedirectToAction("Index", "Exhibitor_Portal");
                    }
                }
            }
        }
    }
}
