﻿using BTT.CaravanQLD.AppPortal.Models;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    [Authorize]
    public class Exhibitor_PortalController : Controller
    {
        private Show GetCurrentShow()
        {
            var showId = (Guid)Session[AppConstants.CurrentShowSession];
            if (showId != null)
            {
                using (var db = new CaravanQLDContext())
                {
                    var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                    return show;
                }
            }
            else
            {
                return null;
            }
        }

        public ActionResult Index()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var directoryPath = Path.Combine(AppConstants.FilesBaseDir, "Exhibitors", currentShow.LinkIdentifier, exhibitor.ShowId);
                if (!System.IO.Directory.Exists(directoryPath))
                    System.IO.Directory.CreateDirectory(directoryPath);
            }

            return View();
        }

        public ActionResult ExhibitorPortalFormList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.Include("FormExhibitors").FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var formGroups = db.FormGroups.Include("Forms").Where(x => x.Forms.Count > 0 && x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var exhibitorPortalPage = db.ContentPages.FirstOrDefault(x => x.IsCurrent && x.PageType == PageType.ExhibitorPortal && x.LinkedShowId == currentShow.Id);
                var model = new ExhibitorPortalFormListModel()
                {
                    Exhibitor = exhibitor,
                    FormGroups = formGroups,
                    Page = exhibitorPortalPage
                };

                return PartialView(model);
            }
        }

        public ActionResult ViewForm(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.Include("FormExhibitors").FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var form = db.Forms.FirstOrDefault(x => x.Id == id);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == form.Id).OrderBy(x => x.Order).ToList();
                var formFieldValues = db.FormFieldValues.ToList().Where(x => formFields.Select(ff => ff.Id).Contains(x.FormFieldId) && x.ExhibitorApplicationId == exhibitor.Id).ToList();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                foreach (var ff in formFields)
                {
                    if (ff.DataType == FormDataType.ContentSection && !String.IsNullOrEmpty(ff.TextContent))
                    {
                        ff.TextContent = AppMethods.ReplaceSmartFields(exhibitor, ff.TextContent, emailSettings);
                    }
                    else
                    {
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (formFieldValue == null)
                        {
                            formFieldValue = new FormFieldValue()
                            {
                                FormFieldId = ff.Id,
                                ExhibitorApplicationId = exhibitor.Id
                            };
                            db.FormFieldValues.Add(formFieldValue);
                            db.SaveChanges();
                        }
                    }
                }

                if (!String.IsNullOrEmpty(form.TextContent))
                    form.TextContent = AppMethods.ReplaceSmartFields(exhibitor, form.TextContent, emailSettings);

                var model = new ViewFormModel()
                {
                    Form = form,
                    Exhibitor = exhibitor,
                    FormFields = formFields,
                    FormFieldValues = db.FormFieldValues.ToList().Where(x => formFields.Select(ff => ff.Id).Contains(x.FormFieldId) && x.ExhibitorApplicationId == exhibitor.Id).ToList(),
                    CurrentShow = currentShow
                };
                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult ViewForm(IEnumerable<HttpPostedFileBase> files, string fakeOptional = "")
        {
            ModelState.Clear();
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var request = Request;
                var formId = Guid.Parse(request.Form.Get("Form.Id"));
                var exhibitor = db.ExhibitorApplications.Include("FormExhibitors").FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var form = db.Forms.FirstOrDefault(x => x.Id == formId);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == form.Id).OrderBy(x => x.Order).ToList();
                var formFieldValues = db.FormFieldValues.ToList().Where(x => formFields.Select(ff => ff.Id).Contains(x.FormFieldId) && x.ExhibitorApplicationId == exhibitor.Id).ToList();

                foreach (var ff in formFields)
                {
                    if (ff.DataType == FormDataType.TextBox || ff.DataType == FormDataType.TextArea)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            ModelState.AddModelError(ff.Id.ToString(), "Please enter a value");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.DropDown)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            ModelState.AddModelError(ff.Id.ToString(), "Please select an option from the list");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.Checkbox)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else
                        {
                            formFieldValue.Value = "False";
                        }
                    }
                    else if (ff.DataType == FormDataType.CheckboxArray)
                    {
                        var formFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == ff.Id).ToList();
                        var newValue = "";
                        for (int i = 0; i < formFieldItems.Count; i++)
                        {
                            var itemValue = request.Form.Get(formFieldItems[i].Id.ToString());
                            if (itemValue == "True" && String.IsNullOrEmpty(newValue))
                            {
                                newValue = String.Format("'{0}'", formFieldItems[i].Value);
                            }
                            else if (itemValue == "True" && !String.IsNullOrEmpty(newValue))
                            {
                                newValue += String.Format("|'{0}'", formFieldItems[i].Value);
                            }
                        }

                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);

                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            ModelState.AddModelError(ff.Id.ToString(), "Please select at least one option from the list below");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.RadioButton)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            ModelState.AddModelError(ff.Id.ToString(), "Please select one of these options");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.FileAttachment)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            ModelState.AddModelError(ff.Id.ToString(), "Select a file");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                }

                db.SaveChanges();

                if (ModelState.Keys.Count == 0)
                {

                    var formExhibitor = exhibitor.FormExhibitors.FirstOrDefault(x => x.FormId == form.Id);
                    if (formExhibitor == null)
                    {
                        formExhibitor = new FormExhibitor()
                        {
                            FormId = form.Id,
                            ExhibitorApplicationId = exhibitor.Id,
                            DateCompleted = DateTime.Now
                        };
                        db.FormExhibitors.Add(formExhibitor);
                    }
                    else
                    {
                        formExhibitor.DateCompleted = DateTime.Now;
                    }

                    db.SaveChanges();
                    exhibitor = db.ExhibitorApplications.Include("FormExhibitors").FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                }

                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                if (!String.IsNullOrEmpty(form.TextContent))
                    form.TextContent = AppMethods.ReplaceSmartFields(exhibitor, form.TextContent, emailSettings);

                var model = new ViewFormModel()
                {
                    Form = form,
                    Exhibitor = exhibitor,
                    FormFields = formFields,
                    FormFieldValues = db.FormFieldValues.ToList().Where(x => formFields.Select(ff => ff.Id).Contains(x.FormFieldId) && x.ExhibitorApplicationId == exhibitor.Id).ToList(),
                    CurrentShow = currentShow
                };

                return PartialView(model);
            }
        }

        public ActionResult MarkContentFormAsViewed(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.Include("FormExhibitors").FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var form = db.Forms.FirstOrDefault(x => x.Id == id);
                var formExhibitor = db.FormExhibitors.FirstOrDefault(x => x.FormId == form.Id && x.ExhibitorApplicationId == exhibitor.Id);
                if (formExhibitor == null)
                {
                    formExhibitor = new FormExhibitor()
                    {
                        DateCompleted = DateTime.Now,
                        FormId = form.Id,
                        ExhibitorApplicationId = exhibitor.Id
                    };
                    db.FormExhibitors.Add(formExhibitor);
                }
                else
                {
                    formExhibitor.DateCompleted = DateTime.Now;
                }

                db.SaveChanges();

                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                if (!String.IsNullOrEmpty(form.TextContent))
                    form.TextContent = AppMethods.ReplaceSmartFields(exhibitor, form.TextContent, emailSettings);

                var model = new ViewFormModel()
                {
                    Form = form,
                    Exhibitor = exhibitor
                };

                return PartialView("ViewForm", model);
            }
        }
    }
}
