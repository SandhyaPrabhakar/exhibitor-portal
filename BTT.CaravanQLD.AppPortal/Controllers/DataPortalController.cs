﻿using BTT.CaravanQLD.AppPortal.Models;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.Model;
using BTT.CsvUtils;
using BTT.JQGrid.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    [Authorize]
    public class DataPortalController : Controller
    {
        private bool IsUserPortalAdmin()
        {
            if (User.Identity.Name == AppConstants.AdminUserName)
                return true;
            else 
                return false;
        }

        private bool HasSelectedShow()
        {
            if (Session[AppConstants.CurrentShowAdminSession] != null)
                return true;
            else
                return false;
        }

        public ActionResult Index()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return RedirectToAction("Exhibitors", "DataPortal");
        }

        #region Exhibitors

        public ActionResult Exhibitors(Guid? ExhibitorId, bool ForwardToExhibitor = false)
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
            {
                var model = new ExhibitorsModel()
                {
                    ForwardToExhibitor = ForwardToExhibitor,
                    ExhibitorId = (Guid)(ExhibitorId == null ? Guid.Empty : ExhibitorId)
                };

                return View(model);
            }
        }

        public ActionResult ExhibitorList()
        {
            return PartialView();
        }

        public ActionResult ExhibitorListForAccepted()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var newExhibitorsToDelete = db.ExhibitorApplications.Where(x => x.ExhibitorEmail == "new@caravanqld.com.au" && x.LinkedShowId == currentShow.Id).ToList();
                foreach (var e in newExhibitorsToDelete)
                {
                    db.ExhibitorApplications.Remove(e);
                    db.SaveChanges();
                }

                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ShowId).ToList();

                var model = new ExhibitorListForAcceptedModel()
                {
                    Exhibitors = exhibitors
                };

                return PartialView(model);
            }
        }

        public ActionResult ExhibitorListForSearch(string showID, string tradingName, string showListingName, string allocatedStandNumber, string siteLocation, string frontage,
            string depth, string area, string addressLine1, string addressLine2, string suburb, string state, string country, string postcode, string exhibitorPhone,
            string exhibitorFax, string exhibitorEmail, string website, string title, string firstName, string surname, string mobile, string phone, string email,
            string moveInDate, string accessGate, string notes)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ShowId).ToList();
                var searchResults = new List<ExhibitorApplication>();

                if (!String.IsNullOrEmpty(showID))
                    searchResults = exhibitors.Where(x => x.ShowId.ToLower().Contains(showID.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(tradingName))
                    searchResults = exhibitors.Where(x => x.TradingName.ToLower().Contains(tradingName.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(showListingName))
                    searchResults = exhibitors.Where(x => x.ShowListingName.ToLower().Contains(showListingName.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(allocatedStandNumber))
                    searchResults = exhibitors.Where(x => x.AllocatedStandNumber.ToLower().Contains(allocatedStandNumber.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(siteLocation))
                    searchResults = exhibitors.Where(x => x.SiteLocation.ToLower().Contains(siteLocation.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(frontage))
                    searchResults = exhibitors.Where(x => x.Frontage.ToLower().Contains(frontage.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(depth))
                    searchResults = exhibitors.Where(x => x.Depth.ToLower().Contains(depth.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(area))
                    searchResults = exhibitors.Where(x => x.Area.ToLower().Contains(area.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(addressLine1))
                    searchResults = exhibitors.Where(x => x.AddressLine1.ToLower().Contains(addressLine1.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(addressLine2))
                    searchResults = exhibitors.Where(x => x.AddressLine2.ToLower().Contains(addressLine2.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(suburb))
                    searchResults = exhibitors.Where(x => x.Suburb.ToLower().Contains(suburb.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(state))
                    searchResults = exhibitors.Where(x => x.State.ToLower().Contains(state.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(country))
                    searchResults = exhibitors.Where(x => x.Country.ToLower().Contains(country.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(postcode))
                    searchResults = exhibitors.Where(x => x.Postcode.ToLower().Contains(postcode.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(exhibitorPhone))
                    searchResults = exhibitors.Where(x => x.ExhibitorPhone.ToLower().Contains(exhibitorPhone.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(exhibitorFax))
                    searchResults = exhibitors.Where(x => x.ExhibitorFax.ToLower().Contains(exhibitorFax.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(exhibitorEmail))
                    searchResults = exhibitors.Where(x => x.ExhibitorEmail.ToLower().Contains(exhibitorEmail.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(website))
                    searchResults = exhibitors.Where(x => x.Website.ToLower().Contains(website.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(title))
                    searchResults = exhibitors.Where(x => x.Title.ToLower().Contains(title.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(firstName))
                    searchResults = exhibitors.Where(x => x.FirstName.ToLower().Contains(firstName.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(surname))
                    searchResults = exhibitors.Where(x => x.Surname.ToLower().Contains(surname.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(mobile))
                    searchResults = exhibitors.Where(x => x.Mobile.ToLower().Contains(mobile.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(phone))
                    searchResults = exhibitors.Where(x => x.Phone.ToLower().Contains(phone.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(email))
                    searchResults = exhibitors.Where(x => x.Email.ToLower().Contains(email.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(moveInDate))
                    searchResults = exhibitors.Where(x => x.MoveInDate.ToLower().Contains(moveInDate.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(accessGate))
                    searchResults = exhibitors.Where(x => x.AccessGate.ToLower().Contains(accessGate.ToLower())).ToList();
                else if (!String.IsNullOrEmpty(notes))
                    searchResults = exhibitors.Where(x => x.Notes.ToLower().Contains(notes.ToLower())).ToList();

                var model = new ExhibitorListForSearchModel()
                {
                    Exhibitors = searchResults
                };

                return PartialView(model);
            }
        }

        public ActionResult EditExhibitor(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == id);

                var model = new EditExhibitorModel()
                {
                    Exhibitor = exhibitor,
                    StateList = AppMethods.CreateListOfStates()
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult EditExhibitor(EditExhibitorModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == model.Exhibitor.Id);
                exhibitor.ShowId = String.IsNullOrEmpty(model.Exhibitor.ShowId) ? "" : model.Exhibitor.ShowId;
                exhibitor.TradingName = String.IsNullOrEmpty(model.Exhibitor.TradingName) ? "" : model.Exhibitor.TradingName;
                exhibitor.ShowListingName = String.IsNullOrEmpty(model.Exhibitor.ShowListingName) ? "" : model.Exhibitor.ShowListingName;
                exhibitor.AllocatedStandNumber = String.IsNullOrEmpty(model.Exhibitor.AllocatedStandNumber) ? "" : model.Exhibitor.AllocatedStandNumber;
                exhibitor.SiteLocation = String.IsNullOrEmpty(model.Exhibitor.SiteLocation) ? "" : model.Exhibitor.SiteLocation;
                exhibitor.Frontage = String.IsNullOrEmpty(model.Exhibitor.Frontage) ? "" : model.Exhibitor.Frontage;
                exhibitor.Depth = String.IsNullOrEmpty(model.Exhibitor.Depth) ? "" : model.Exhibitor.Depth;
                exhibitor.Area = String.IsNullOrEmpty(model.Exhibitor.Area) ? "" : model.Exhibitor.Area;
                exhibitor.AddressLine1 = String.IsNullOrEmpty(model.Exhibitor.AddressLine1) ? "" : model.Exhibitor.AddressLine1;
                exhibitor.AddressLine2 = String.IsNullOrEmpty(model.Exhibitor.AddressLine2) ? "" : model.Exhibitor.AddressLine2;
                exhibitor.Suburb = String.IsNullOrEmpty(model.Exhibitor.Suburb) ? "" : model.Exhibitor.Suburb;
                exhibitor.State = String.IsNullOrEmpty(model.Exhibitor.State) ? "" : model.Exhibitor.State;
                exhibitor.Country = String.IsNullOrEmpty(model.Exhibitor.Country) ? "" : model.Exhibitor.Country;
                exhibitor.Postcode = String.IsNullOrEmpty(model.Exhibitor.Postcode) ? "" : model.Exhibitor.Postcode;
                exhibitor.ExhibitorPhone = String.IsNullOrEmpty(model.Exhibitor.ExhibitorPhone) ? "" : model.Exhibitor.ExhibitorPhone;
                exhibitor.ExhibitorFax = String.IsNullOrEmpty(model.Exhibitor.ExhibitorFax) ? "" : model.Exhibitor.ExhibitorFax;
                exhibitor.ExhibitorEmail = String.IsNullOrEmpty(model.Exhibitor.ExhibitorEmail) ? "" : model.Exhibitor.ExhibitorEmail;
                exhibitor.Website = String.IsNullOrEmpty(model.Exhibitor.Website) ? "" : model.Exhibitor.Website;
                exhibitor.Title = String.IsNullOrEmpty(model.Exhibitor.Title) ? "" : model.Exhibitor.Title;
                exhibitor.FirstName = String.IsNullOrEmpty(model.Exhibitor.FirstName) ? "" : model.Exhibitor.FirstName;
                exhibitor.Surname = String.IsNullOrEmpty(model.Exhibitor.Surname) ? "" : model.Exhibitor.Surname;
                exhibitor.Mobile = String.IsNullOrEmpty(model.Exhibitor.Mobile) ? "" : model.Exhibitor.Mobile;
                exhibitor.Phone = String.IsNullOrEmpty(model.Exhibitor.Phone) ? "" : model.Exhibitor.Phone;
                exhibitor.Email = String.IsNullOrEmpty(model.Exhibitor.Email) ? "" : model.Exhibitor.Email;
                exhibitor.MoveInDate = String.IsNullOrEmpty(model.Exhibitor.MoveInDate) ? "" : model.Exhibitor.MoveInDate;
                exhibitor.AccessTime = String.IsNullOrEmpty(model.Exhibitor.AccessTime) ? "" : model.Exhibitor.AccessTime;
                exhibitor.AccessGate = String.IsNullOrEmpty(model.Exhibitor.AccessGate) ? "" : model.Exhibitor.AccessGate;
                exhibitor.Notes = String.IsNullOrEmpty(model.Exhibitor.Notes) ? "" : model.Exhibitor.Notes;
                db.SaveChanges();

                model.StateList = AppMethods.CreateListOfStates();
                return PartialView(model);
            }
        }

        public ActionResult DeleteExhibitor(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var exhibitor = db.ExhibitorApplications.Include("EmailLogs").Include("ExhibitorApplicationEmails").FirstOrDefault(x => x.Id == id);

                for (int i = exhibitor.ExhibitorApplicationEmails.Count - 1; i >= 0; i--)
                {
                    db.ExhibitorApplicationEmails.Remove(exhibitor.ExhibitorApplicationEmails.ToList()[i]);
                }

                for (int i = exhibitor.EmailLogs.Count - 1; i >= 0; i--)
                {
                    db.EmailLogs.Remove(exhibitor.EmailLogs.ToList()[i]);
                }

                db.ExhibitorApplications.Remove(exhibitor);
                db.SaveChanges();

                return PartialView("ExhibitorList");
            }
        }

        public ActionResult AddNewExhibitor()
        {
            using (var db = new CaravanQLDContext())
            {
                var exhibitor = new ExhibitorApplication();
                exhibitor.ShowId = GetNextShowID();
                exhibitor.DateAdded = DateTime.Now;
                exhibitor.LinkedShowId = GetCurrentShow().Id;

                var model = new AddNewExhibitorModel()
                {
                    Exhibitor = exhibitor,
                    StateList = AppMethods.CreateListOfStates()
                };

                return PartialView("AddNewExhibitor", model);
            }
        }

        [HttpPost]
        public ActionResult AddNewExhibitor(AddNewExhibitorModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var exhibitor = new ExhibitorApplication();
                exhibitor.ShowId = model.Exhibitor.ShowId;
                exhibitor.TradingName = model.Exhibitor.TradingName;
                exhibitor.ShowListingName = model.Exhibitor.ShowListingName;
                exhibitor.AllocatedStandNumber = model.Exhibitor.AllocatedStandNumber;
                exhibitor.SiteLocation = model.Exhibitor.SiteLocation;
                exhibitor.Frontage = model.Exhibitor.Frontage;
                exhibitor.Depth = model.Exhibitor.Depth;
                exhibitor.Area = model.Exhibitor.Area;
                exhibitor.AddressLine1 = model.Exhibitor.AddressLine1;
                exhibitor.AddressLine2 = model.Exhibitor.AddressLine2;
                exhibitor.Suburb = model.Exhibitor.Suburb;
                exhibitor.State = model.Exhibitor.State;
                exhibitor.Country = model.Exhibitor.Country;
                exhibitor.Postcode = model.Exhibitor.Postcode;
                exhibitor.ExhibitorPhone = model.Exhibitor.ExhibitorPhone;
                exhibitor.ExhibitorFax = model.Exhibitor.ExhibitorFax;
                exhibitor.ExhibitorEmail = model.Exhibitor.ExhibitorEmail;
                exhibitor.Website = model.Exhibitor.Website;
                exhibitor.Title = model.Exhibitor.Title;
                exhibitor.FirstName = model.Exhibitor.FirstName;
                exhibitor.Surname = model.Exhibitor.Surname;
                exhibitor.Mobile = model.Exhibitor.Mobile;
                exhibitor.Phone = model.Exhibitor.Phone;
                exhibitor.Email = model.Exhibitor.Email;
                exhibitor.MoveInDate = model.Exhibitor.MoveInDate;
                exhibitor.AccessTime = model.Exhibitor.AccessTime;
                exhibitor.AccessGate = model.Exhibitor.AccessGate;
                exhibitor.Notes = model.Exhibitor.Notes;
                exhibitor.LinkedShowId = GetCurrentShow().Id;
                db.ExhibitorApplications.Add(exhibitor);
                db.SaveChanges();

                ModelState.Clear();

                var newExhibitor = new ExhibitorApplication();
                newExhibitor.ShowId = GetNextShowID();
                newExhibitor.DateAdded = DateTime.Now;
                newExhibitor.LinkedShowId = GetCurrentShow().Id;

                model = new AddNewExhibitorModel()
                {
                    Exhibitor = newExhibitor,
                    StateList = AppMethods.CreateListOfStates()
                };

                return PartialView("AddNewExhibitor", model);
            }
        }

        public string GetNextShowID()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var showIDs = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).Select(x => x.ShowId.Remove(0, 3)).ToList();
                if (showIDs.Count > 0)
                {
                    var numberList = new List<int>();
                    foreach (var num in showIDs)
                    {
                        numberList.Add(Convert.ToInt32(num));
                    }

                    return "CQS" + (numberList.Max() + 1).ToString();
                }
                else
                    return "CQS1";
            }
        }

        public ActionResult ViewExhibitorFormStats(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == id);
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var formExhibitors = db.FormExhibitors.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();
                var completedForms = forms.Where(x => formExhibitors.Select(z => z.FormId).Contains(x.Id)).OrderBy(x => x.Order).ToList();
                var uncompletedForms = forms.Where(x => !formExhibitors.Select(z => z.FormId).Contains(x.Id)).OrderBy(x => x.Order).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                var model = new ViewExhibitorFormStatsModel()
                {
                    Exhibitor = exhibitor,
                    FormExhibitors = formExhibitors,
                    CompletedForms = completedForms,
                    UncompletedForms = uncompletedForms,
                    EmailLogsForExhibitor = emailLogs
                };

                return PartialView(model);
            }
        }

        #endregion

        #region Imports

        public ActionResult Imports()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return View();
        }

        public ActionResult ImportNewExhibitors()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult ImportNewExhibitors(string fakeOptionalParam = "")
        {
            var currentShow = GetCurrentShow();
            var file = Request.Files[0];
            if (file != null && file.ContentLength > 0)
            {
                if (Path.GetExtension(file.FileName) == ".csv")
                {
                    using (CsvReader reader = new CsvReader(file.InputStream))
                    {
                        using (var db = new CaravanQLDContext())
                        {
                            foreach (string[] elements in reader.RowEnumerator)
                            {
                                if (reader.RowIndex == 1) //skip the first row with column headings
                                {
                                    continue;
                                }

                                if (!String.IsNullOrEmpty(elements[0]))
                                {
                                    var exhibitorApp = new ExhibitorApplication();
                                    exhibitorApp.ShowId = elements[0];
                                    exhibitorApp.TradingName = elements[1];
                                    exhibitorApp.ShowListingName = elements[2];
                                    exhibitorApp.AllocatedStandNumber = elements[3];
                                    exhibitorApp.SiteLocation = elements[4];
                                    exhibitorApp.Frontage = elements[5];
                                    exhibitorApp.Depth = elements[6];
                                    exhibitorApp.Area = elements[7];
                                    exhibitorApp.AddressLine1 = elements[8];
                                    exhibitorApp.AddressLine2 = elements[9];
                                    exhibitorApp.Suburb = elements[10];
                                    exhibitorApp.State = elements[11];
                                    exhibitorApp.Country = elements[12];
                                    exhibitorApp.Postcode = elements[13];
                                    exhibitorApp.ExhibitorPhone = elements[14];
                                    exhibitorApp.ExhibitorFax = elements[15];
                                    exhibitorApp.ExhibitorEmail = elements[16];
                                    exhibitorApp.Website = elements[17];
                                    exhibitorApp.Title = elements[18];
                                    exhibitorApp.FirstName = elements[19];
                                    exhibitorApp.Surname = elements[20];
                                    exhibitorApp.Phone = elements[21];
                                    exhibitorApp.Mobile = elements[22];
                                    exhibitorApp.Email = elements[23];
                                    exhibitorApp.LinkedShowId = currentShow.Id;
                                    db.ExhibitorApplications.Add(exhibitorApp);

                                }
                            }
                            db.SaveChanges();
                        }
                    }
                }
            }

            return RedirectToAction("Exhibitors", "DataPortal");
        }

        public ActionResult UpdateExhibitors()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult UpdateExhibitors(string fakeOptionalParam = "")
        {
            var currentShow = GetCurrentShow();
            var file = Request.Files[0];
            if (file != null && file.ContentLength > 0)
            {
                if (Path.GetExtension(file.FileName) == ".csv")
                {
                    using (CsvReader reader = new CsvReader(file.InputStream))
                    {
                        using (var db = new CaravanQLDContext())
                        {
                            foreach (string[] elements in reader.RowEnumerator)
                            {
                                if (reader.RowIndex == 1) //skip the first row with column headings
                                {
                                    continue;
                                }

                                if (!String.IsNullOrEmpty(elements[0]))
                                {
                                    var showId = elements[0];
                                    var existingExhibitorApp = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == showId && x.LinkedShowId == currentShow.Id);
                                    if (existingExhibitorApp != null)
                                    {
                                        existingExhibitorApp.MoveInDate = elements[1];
                                        existingExhibitorApp.AccessTime = elements[2];
                                        existingExhibitorApp.AccessGate = elements[3];
                                        existingExhibitorApp.Notes = elements[4];
                                    }

                                }
                            }
                            db.SaveChanges();
                        }
                    }
                }
            }

            return RedirectToAction("Exhibitors", "DataPortal");
        }

        public ActionResult ExportAllData()
        {
            var fileName = String.Format("All Exhibitors {0}.csv", DateTime.Now.ToString("dd-MM-yyyy hh-mm-ss"));
            var filePath = Path.Combine(Request.PhysicalApplicationPath, "Files", "files", fileName);
            using (var file = System.IO.File.Open(filePath, FileMode.CreateNew))
            {
                using (var csvWriter = new CsvWriter(file))
                {
                    csvWriter.Write(CreateHeaderRow());

                    using (var db = new CaravanQLDContext())
                    {
                        var currentShow = GetCurrentShow();
                        var exApps = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ToList();
                        foreach (var exApp in exApps)
                        {
                            try
                            {
                                csvWriter.Write(CreateExhibitorRow(exApp));
                            }
                            catch (Exception exc)
                            {

                            }
                        }
                    }
                }
            }

            return File(filePath, "application/csv", fileName);
        }

        private string[] CreateHeaderRow()
        {
            string[] fields = new string[28];
            fields[0] = "Show ID";
            fields[1] = "Trading Name";
            fields[2] = "Show Listing Name";
            fields[3] = "Allocated Stand Number";
            fields[4] = "Site Location";
            fields[5] = "Frontage";
            fields[6] = "Depth";
            fields[7] = "Area";
            fields[8] = "Address Line 1";
            fields[9] = "Address Line 2";
            fields[10] = "Suburb / Town";
            fields[11] = "State";
            fields[12] = "Country";
            fields[13] = "Postcode";
            fields[14] = "Exhibitor Phone";
            fields[15] = "Exhibitor Fax";
            fields[16] = "Exhibitor Email";
            fields[17] = "Website";
            fields[18] = "Title / Position";
            fields[19] = "First Name";
            fields[20] = "Surname";
            fields[21] = "Mobile";
            fields[22] = "Phone";
            fields[23] = "Email";
            fields[24] = "Move In Date";
            fields[25] = "Access Time";
            fields[26] = "Access Gate";
            fields[27] = "Notes";

            return fields;
        }

        private string[] CreateExhibitorRow(ExhibitorApplication exApp)
        {
            string[] fields = new string[28];
            fields[0] = exApp.ShowId;
            fields[1] = CheckStringForNull(exApp.TradingName);
            fields[2] = CheckStringForNull(exApp.ShowListingName);
            fields[3] = CheckStringForNull(exApp.AllocatedStandNumber);
            fields[4] = CheckStringForNull(exApp.SiteLocation);
            fields[5] = CheckStringForNull(exApp.Frontage);
            fields[6] = CheckStringForNull(exApp.Depth);
            fields[7] = CheckStringForNull(exApp.Area);
            fields[8] = CheckStringForNull(exApp.AddressLine1);
            fields[9] = CheckStringForNull(exApp.AddressLine2);
            fields[10] = CheckStringForNull(exApp.Suburb);
            fields[11] = CheckStringForNull(exApp.State);
            fields[12] = CheckStringForNull(exApp.Country);
            fields[13] = CheckStringForNull(exApp.Postcode);
            fields[14] = CheckStringForNull(exApp.ExhibitorPhone);
            fields[15] = CheckStringForNull(exApp.ExhibitorFax);
            fields[16] = CheckStringForNull(exApp.ExhibitorEmail);
            fields[17] = CheckStringForNull(exApp.Website);
            fields[18] = CheckStringForNull(exApp.Title);
            fields[19] = CheckStringForNull(exApp.FirstName);
            fields[20] = CheckStringForNull(exApp.Surname);
            fields[21] = CheckStringForNull(exApp.Mobile);
            fields[22] = CheckStringForNull(exApp.Phone);
            fields[23] = CheckStringForNull(exApp.Email);
            fields[24] = CheckStringForNull(exApp.MoveInDate);
            fields[25] = CheckStringForNull(exApp.AccessTime);
            fields[26] = CheckStringForNull(exApp.AccessGate);
            fields[27] = CheckStringForNull(exApp.Notes);

            return fields;
        }

        private string ConvertBoolToYesNo(bool value)
        {
            if (value)
                return "Yes";
            else
                return "No";
        }

        private string CheckStringForNull(string value)
        {
            if (String.IsNullOrEmpty(value))
                return "";
            else
                return value;
        }

        #endregion

        #region Emails

        public ActionResult Emails(Guid? EmailId, bool ForwardToEmail = false)
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
            {
                var model = new EmailsModel()
                {
                    ForwardToEmail = ForwardToEmail,
                    EmailId = (Guid)(EmailId == null ? Guid.Empty : EmailId)
                };

                return View(model);
        }
        }

        public ActionResult EmailList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                var model = new EmailListModel()
                {
                    SortByDateCreated = emailSettings.SortByDateCreated
                };

                return PartialView(model);
            }
        }

        public ActionResult ChangeEmailListSortOrder(bool SortByDateCreated)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                emailSettings.SortByDateCreated = SortByDateCreated;
                db.SaveChanges();

                var model = new EmailListModel()
                {
                    SortByDateCreated = emailSettings.SortByDateCreated
                };

                return PartialView("EmailList", model);
            }
        }

        public ActionResult EmailListByDateCreated()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).OrderByDescending(x => x.DateCreated).ToList();
                var monthList = new List<DateTime>();
                //Get the current month and remove all days, hours, minutes, seconds, and milliseconds
                var timeNow = DateTime.Now;
                var timeToSubtract = new TimeSpan(timeNow.Day - 1, timeNow.Hour, timeNow.Minute, timeNow.Second, timeNow.Millisecond);
                var thisMonth = timeNow.Subtract(timeToSubtract);

                monthList.Add(thisMonth);
                //Add the preceeding 11 months to the month list
                for (int i = 1; i < 12; i++)
                {
                    monthList.Add(thisMonth.AddMonths(-i));
                }

                var model = new EmailListByDateCreated()
                {
                    Emails = emails,
                    Last12Months = monthList
                };

                return PartialView(model);
            }
        }

        public ActionResult EmailListBySubject()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Subject).ToList();
                var model = new EmailListBySubjectModel()
                {
                    Emails = emails
                };

                return PartialView(model);
            }
        }

        public ActionResult CreateNewEmail()
        {
            using (var db = new CaravanQLDContext())
            {
                var email = new Email();
                email.LinkedShowId = GetCurrentShow().Id;
                db.Emails.Add(email);
                db.SaveChanges();

                var model = new EditEmailModel()
                {
                    Email = email
                };

                return PartialView("EditEmail", model);
            }
        }

        public ActionResult SetInitialEmail(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                foreach (var email in emails)
                {
                    email.IsInitialEmail = false;
                }

                var initialEmail = emails.FirstOrDefault(x => x.Id == id);
                initialEmail.IsInitialEmail = true;
                db.SaveChanges();

                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                var model = new EmailListModel()
                {
                    SortByDateCreated = emailSettings.SortByDateCreated
                };

                return PartialView("EmailList", model);
            }
        }

        public ActionResult EditEmail(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var model = new EditEmailModel()
                {
                    Email = email
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditEmail(EditEmailModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new CaravanQLDContext())
                {
                    var currentShow = GetCurrentShow();
                    var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList();

                    //If this is now the initial email, set all others to false
                    if (model.Email.IsInitialEmail)
                    {
                        foreach (var e in emails)
                        {
                            e.IsInitialEmail = false;
                        }
                    }

                    var email = emails.FirstOrDefault(x => x.Id == model.Email.Id);
                    email.Subject = model.Email.Subject;
                    email.Body = model.Email.Body;
                    email.IsInitialEmail = model.Email.IsInitialEmail;
                    db.SaveChanges();
                }
            }

            return PartialView(model);
        }

        public ActionResult SendTestEmail(Guid id)
        {
            try
            {
                using (var db = new CaravanQLDContext())
                {
                    var email = db.Emails.FirstOrDefault(x => x.Id == id);
                    if (email != null)
                    {
                        var currentShow = GetCurrentShow();
                        var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                        if (!String.IsNullOrEmpty(emailSettings.TestEmailAddress))
                        {
                            //Get the test exhibitor
                            var testExhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == emailSettings.TestExhibitorShowID && x.LinkedShowId == currentShow.Id);

                            #if Debug
                                SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
                            #else
                                SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                                client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
                            #endif
                            MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                            MailAddress to = new MailAddress(emailSettings.TestEmailAddress);
                            MailMessage message = new MailMessage(from, to);
                            var newBody = AppMethods.ReplaceSmartFields(testExhibitor, email.Body, emailSettings) + GetEmailFooter();
                            message.Body = newBody;
                            message.Subject = email.Subject;
                            message.IsBodyHtml = true;
                            client.Send(message);
                        }
                    }
                };
            }
            catch (Exception exc)
            {
                //Triggers the failure message
                return Json(new { success = false });
            }

            //Triggers the success message
            var model = new EmailsModel()
            {
                ForwardToEmail = false,
                EmailId = Guid.Empty
            };
            return PartialView("Emails", model);
        }

        public ActionResult SendEmail(Guid id)
        {
            var success = true;

            using (var db = new CaravanQLDContext())
            {
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                if (email != null)
                {
                    var currentShow = GetCurrentShow();
                    var emailExs = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).Select(x => x.ExhibitorApplicationId);
                    var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    var exhibitors = db.ExhibitorApplications.Where(x => emailExs.Contains(x.Id));
                    var failedExhibitors = new List<ExhibitorApplication>();

                    foreach (var ex in exhibitors)
                    {
                        try
                        {
                            #if Debug
                                SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
                            #else
                                SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                                client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
                            #endif
                            MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                            MailAddress to = new MailAddress(ex.Email);
                            MailMessage message = new MailMessage(from, to);
                            var newBody = AppMethods.ReplaceSmartFields(ex, email.Body, emailSettings) + GetEmailFooter();
                            message.Body = newBody;
                            message.Subject = email.Subject;
                            message.IsBodyHtml = true;
                            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                            client.Send(message);

                            var emailLog = new EmailLog()
                            {
                                DateSent = DateTime.Now,
                                EmailId = id,
                                ExhibitorApplicationId = ex.Id,
                                LinkedShowId = currentShow.Id
                            };

                            db.EmailLogs.Add(emailLog);
                        }
                        catch (Exception exc)
                        {
                            failedExhibitors.Add(ex);
                        }
                    }

                    db.SaveChanges();

                    //Send an email to Caravan Queensland with the results of the email
                    var completionEmail = email.Subject + " has been sent.";
                    if (failedExhibitors.Count <= 0)
                    {
                        completionEmail += "<p>There were no problems sending this email.</p>";
                    }
                    else
                    {
                        completionEmail += "<p>The email failed to send to the following exhibitors.</p>";
                        foreach (var failedEx in failedExhibitors)
                        {
                            completionEmail += "<br />" + (!String.IsNullOrEmpty(failedEx.ShowListingName) ? failedEx.ShowListingName : failedEx.Email);
                        }
                    }

                    #if Debug
                        SmtpClient client2 = new SmtpClient("smtp.virginbroadband.com.au");
                    #else
                        SmtpClient client2 = new SmtpClient(AppConstants.SmptHostName, 587);
                        client2.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
                    #endif
                    MailAddress from2 = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                    MailAddress to2 = new MailAddress(emailSettings.AdminEmailAddress);
                    MailMessage message2 = new MailMessage(from2, to2);
                    message2.Body = completionEmail;
                    message2.Subject = "Email Report - " + email.Subject;
                    message2.IsBodyHtml = true;
                    client2.Send(message2);
                }
            };

            //Triggers the failure message or Triggers the success message
            if (!success)
                return Json(new { success = false });
            else
            {
                var model = new EmailsModel()
                {
                    ForwardToEmail = false,
                    EmailId = Guid.Empty
                };
                return PartialView("Emails", model);
            }
        }

        public ActionResult SendOneEmail(Guid EmailId, Guid ExhibitorId)
        {
            var success = true;

            using (var db = new CaravanQLDContext())
            {
                var email = db.Emails.FirstOrDefault(x => x.Id == EmailId);
                if (email != null)
                {
                    var currentShow = GetCurrentShow();
                    var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == ExhibitorId);
                    var failedExhibitors = new List<ExhibitorApplication>();

                    try
                    {
                        #if Debug
                            SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
                        #else
                            SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                            client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
                        #endif
                        MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                        MailAddress to = new MailAddress(exhibitor.Email);
                        MailMessage message = new MailMessage(from, to);
                        var newBody = AppMethods.ReplaceSmartFields(exhibitor, email.Body, emailSettings) + GetEmailFooter();
                        message.Body = newBody;
                        message.Subject = email.Subject;
                        message.IsBodyHtml = true;
                        message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                        client.Send(message);

                        var emailLog = new EmailLog()
                        {
                            DateSent = DateTime.Now,
                            EmailId = EmailId,
                            ExhibitorApplicationId = ExhibitorId,
                            LinkedShowId = currentShow.Id
                        };

                        db.EmailLogs.Add(emailLog);
                    }
                    catch (Exception exc)
                    {
                        success = false;
                        failedExhibitors.Add(exhibitor);
                    }

                    db.SaveChanges();

                    //Send an email to Caravan Queensland with the results of the email
                    //var completionEmail = email.Subject + " has been sent.";
                    //if (failedExhibitors.Count <= 0)
                    //{
                    //    completionEmail += "<p>There were no problems sending this email.</p>";
                    //}
                    //else
                    //{
                    //    completionEmail += "<p>The email failed to send to the following exhibitors.</p>";
                    //    foreach (var failedEx in failedExhibitors)
                    //    {
                    //        completionEmail += "<br />" + (!String.IsNullOrEmpty(failedEx.ShowListingName) ? failedEx.ShowListingName : failedEx.Email);
                    //    }
                    //}

                    //#if Debug
                    //    SmtpClient client2 = new SmtpClient("smtp.virginbroadband.com.au");
                    //#else
                    //SmtpClient client2 = new SmtpClient(AppConstants.SmptHostName);
                    //client2.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
                    //#endif
                    //MailAddress from2 = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                    //MailAddress to2 = new MailAddress(emailSettings.AdminEmailAddress);
                    //MailMessage message2 = new MailMessage(from2, to2);
                    //message2.Body = completionEmail;
                    //message2.Subject = "Email Report - " + email.Subject;
                    //message2.IsBodyHtml = true;
                    //client2.Send(message2);
                }
            };

            //Triggers the failure message or Triggers the success message
            if (!success)
                return Json(new { success = false });
            else
            {
                var model = new EmailsModel()
                {
                    ForwardToEmail = false,
                    EmailId = Guid.Empty
                };
                return PartialView("Emails", model);
            }
        }

        private string GetEmailFooter()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                var emailFooter = String.Format("<hr /><p align='center' style='color:#A9A9A9;'>{0}<br />Phone: {1} | Fax: {2} | {3} <br /> Email: <a href='mailto:{4}'>{4}</a> | Web: <a href='{5}'>{5}</a></p>",
                    emailSettings.CompanyName, emailSettings.ContactNumber, emailSettings.ContactFax, emailSettings.ContactPostalAddress, emailSettings.ContactEmail, emailSettings.Website);

                return emailFooter;
            }
        }

        public ActionResult DeleteEmail(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                var forms = db.Forms.Where(x => x.EmailId == email.Id).ToList();
                foreach (var form in forms)
                {
                    form.EmailId = null;
                }

                db.Emails.Remove(email);
                db.SaveChanges();

                var currentShow = GetCurrentShow();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                var model = new EmailListModel()
                {
                    SortByDateCreated = emailSettings.SortByDateCreated
                };

                return PartialView("EmailList", model);
            }
        }

        public ActionResult ClearEmailLogs(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }
                db.SaveChanges();

                var model = new LogListForEmailModel()
                {
                    EmailLogs = new List<EmailLog>(),
                    EmailId = id
                };

                return PartialView("LogListForEmail", model);
            }
        }

        public ActionResult ClearAllEmailLogs()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }
                db.SaveChanges();

                var model = new LogListForAllModel()
                {
                    EmailLogs = new List<EmailLog>()
                };

                return PartialView("LogListForAll", model);
            }
        }

        public ActionResult ClearExhibitorLogs(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var emailLogs = db.EmailLogs.Where(x => x.ExhibitorApplicationId == id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }
                db.SaveChanges();

                var model = new LogListForExhibitorModel()
                {
                    EmailLogs = new List<EmailLog>(),
                    ExhibitorId = id
                };

                return PartialView("LogListForExhibitor", model);
            }
        }

        public ActionResult ChangeEmailsExhibitors(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id).OrderBy(x => x.DateSent).ToList();

                var model = new ChangeEmailsExhibitors()
                {
                    CurrentEmail = email,
                    Exhibitors = exhibitors,
                    ExhibitorApplicationEmails = exhibitorEmails,
                    EmailLogs = emailLogs
                };

                return PartialView(model);
            }
        }

        public ActionResult AddExhibitorToEmail(Guid EmailId, Guid ExhibitorId)
        {
            using (var db = new CaravanQLDContext())
            {
                var existingEmailEx = db.ExhibitorApplicationEmails.FirstOrDefault(x => x.EmailId == EmailId && x.ExhibitorApplicationId == ExhibitorId);

                if (existingEmailEx == null)
                {
                    var emailEx = new ExhibitorApplicationEmail()
                    {
                        EmailId = EmailId,
                        ExhibitorApplicationId = ExhibitorId
                    };

                    db.ExhibitorApplicationEmails.Add(emailEx);
                }
                else
                {
                    db.ExhibitorApplicationEmails.Remove(existingEmailEx);
                }

                db.SaveChanges();

                var currentShow = GetCurrentShow();
                var email = db.Emails.FirstOrDefault(x => x.Id == EmailId);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == EmailId).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id).OrderBy(x => x.DateSent).ToList();

                var model = new ChangeEmailsExhibitors()
                {
                    CurrentEmail = email,
                    Exhibitors = exhibitors,
                    ExhibitorApplicationEmails = exhibitorEmails,
                    EmailLogs = emailLogs
                };

                return PartialView("ChangeEmailsExhibitors", model);
            }
        }

        public ActionResult SelectAllExhibitors(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id).OrderBy(x => x.DateSent).ToList();

                foreach (var exhibitor in exhibitors)
                {
                    var existingEmailEx = exhibitorEmails.FirstOrDefault(x => x.EmailId == id && x.ExhibitorApplicationId == exhibitor.Id);

                    if (existingEmailEx == null)
                    {
                        var emailEx = new ExhibitorApplicationEmail()
                        {
                            EmailId = id,
                            ExhibitorApplicationId = exhibitor.Id
                        };

                        db.ExhibitorApplicationEmails.Add(emailEx);
                    }
                }

                db.SaveChanges();
                exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();

                var model = new ChangeEmailsExhibitors()
                {
                    CurrentEmail = email,
                    Exhibitors = exhibitors,
                    ExhibitorApplicationEmails = exhibitorEmails,
                    EmailLogs = emailLogs
                };

                return PartialView("ChangeEmailsExhibitors", model);
            }
        }

        public ActionResult DeselectAllExhibitors(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id).OrderBy(x => x.DateSent).ToList();

                foreach (var exhibitor in exhibitors)
                {
                    var existingEmailEx = exhibitorEmails.FirstOrDefault(x => x.EmailId == id && x.ExhibitorApplicationId == exhibitor.Id);

                    if (existingEmailEx != null)
                    {
                        db.ExhibitorApplicationEmails.Remove(existingEmailEx);
                    }
                }

                db.SaveChanges();
                exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();

                var model = new ChangeEmailsExhibitors()
                {
                    CurrentEmail = email,
                    Exhibitors = exhibitors,
                    ExhibitorApplicationEmails = exhibitorEmails,
                    EmailLogs = emailLogs
                };

                return PartialView("ChangeEmailsExhibitors", model);
            }
        }

        public ActionResult LogListForExhibitor(Guid id, int page = 1)
        {
            using (var db = new CaravanQLDContext())
            {
                var emailLogs = db.EmailLogs.Include("ExhibitorApplication").Include("Email").Where(x => x.ExhibitorApplicationId == id).OrderBy(x => x.ExhibitorApplication.TradingName).ThenByDescending(x => x.DateSent).ToList();
                var currentShow = GetCurrentShow();
                var numLogs = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id).LogsPerPage;

                var model = new LogListForExhibitorModel()
                {
                    CurrentPage = page,
                    TotalRecords = emailLogs.Count,
                    LogsPerPage = numLogs,
                    ExhibitorId = id,
                    EmailLogs = emailLogs.GetRange(((page - 1) * numLogs), (emailLogs.Count > page * numLogs ? numLogs : (emailLogs.Count - ((page - 1) * numLogs))))
                };

                return PartialView(model);
            }
        }

        public ActionResult LogListForEmail(Guid id, int page = 1)
        {
            using (var db = new CaravanQLDContext())
            {
                var emailLogs = db.EmailLogs.Include("ExhibitorApplication").Include("Email").Where(x => x.EmailId == id).OrderBy(x => x.ExhibitorApplication.TradingName).ThenByDescending(x => x.DateSent).ToList();
                var currentShow = GetCurrentShow();
                var numLogs = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id).LogsPerPage;

                var model = new LogListForEmailModel()
                {
                    CurrentPage = page,
                    TotalRecords = emailLogs.Count,
                    LogsPerPage = numLogs,
                    EmailId = id,
                    EmailLogs = emailLogs.GetRange(((page - 1) * numLogs), (emailLogs.Count > page * numLogs ? numLogs : (emailLogs.Count - ((page - 1) * numLogs))))
                };

                return PartialView(model);
            }
        }

        public ActionResult LogListForAll(int page = 1)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailLogs = db.EmailLogs.Include("ExhibitorApplication").Include("Email").Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.ExhibitorApplication.TradingName).ThenByDescending(x => x.DateSent).ToList();
                var numLogs = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id).LogsPerPage;

                var model = new LogListForAllModel()
                {
                    CurrentPage = page,
                    TotalRecords = emailLogs.Count,
                    LogsPerPage = numLogs,
                    EmailLogs = emailLogs.GetRange(((page - 1) * numLogs), (emailLogs.Count > page * numLogs ? numLogs : (emailLogs.Count - ((page - 1) * numLogs))))
                };

                return PartialView(model);
            }
        }

        public ActionResult LogListForSearch(string searchType, Guid searchId, string date, string subject, string tradingName, string contactEmail)
        {
            using (var db = new CaravanQLDContext())
            {
                var searchResult = new List<EmailLog>();

                if (searchType == "email")
                {
                    var emailLogs = db.EmailLogs.Include("ExhibitorApplication").Include("Email").Where(x => x.EmailId == searchId).OrderBy(x => x.ExhibitorApplication.TradingName).ThenByDescending(x => x.DateSent).ToList();

                    if (!String.IsNullOrEmpty(date))
                    {
                        var searchDate = DateTime.Parse(date);
                        searchResult = emailLogs.Where(x => x.DateSent >= searchDate && x.DateSent < searchDate.AddDays(1)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(subject))
                    {
                        searchResult = emailLogs.Where(x => x.Email.Subject.ToLower().Contains(subject.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(tradingName))
                    {
                        searchResult = emailLogs.Where(x => x.ExhibitorApplication.TradingName.ToLower().Contains(tradingName.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(contactEmail))
                    {
                        searchResult = emailLogs.Where(x => x.ExhibitorApplication.Email.ToLower().Contains(contactEmail.ToLower())).ToList();
                    }
                }
                else if (searchType == "exhibitor")
                {
                    var emailLogs = db.EmailLogs.Include("ExhibitorApplication").Include("Email").Where(x => x.ExhibitorApplication.Id == searchId).OrderBy(x => x.ExhibitorApplication.TradingName).ThenByDescending(x => x.DateSent).ToList();

                    if (!String.IsNullOrEmpty(date))
                    {
                        var searchDate = DateTime.Parse(date);
                        searchResult = emailLogs.Where(x => x.DateSent >= searchDate && x.DateSent < searchDate.AddDays(1)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(subject))
                    {
                        searchResult = emailLogs.Where(x => x.Email.Subject.ToLower().Contains(subject.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(tradingName))
                    {
                        searchResult = emailLogs.Where(x => x.ExhibitorApplication.TradingName.ToLower().Contains(tradingName.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(contactEmail))
                    {
                        searchResult = emailLogs.Where(x => x.ExhibitorApplication.Email.ToLower().Contains(contactEmail.ToLower())).ToList();
                    }
                }
                else if (searchType == "all")
                {
                    var currentShow = GetCurrentShow();
                    var emailLogs = db.EmailLogs.Include("ExhibitorApplication").Include("Email").Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.ExhibitorApplication.TradingName).ThenByDescending(x => x.DateSent).ToList();

                    if (!String.IsNullOrEmpty(date))
                    {
                        var searchDate = DateTime.Parse(date);
                        searchResult = emailLogs.Where(x => x.DateSent >= searchDate && x.DateSent < searchDate.AddDays(1)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(subject))
                    {
                        searchResult = emailLogs.Where(x => x.Email.Subject.ToLower().Contains(subject.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(tradingName))
                    {
                        searchResult = emailLogs.Where(x => x.ExhibitorApplication.TradingName.ToLower().Contains(tradingName.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(contactEmail))
                    {
                        searchResult = emailLogs.Where(x => x.ExhibitorApplication.Email.ToLower().Contains(contactEmail.ToLower())).ToList();
                    }
                }

                var model = new LogListForSearchModel()
                {
                    SearchId = searchId,
                    SearchType = searchType,
                    EmailLogs = searchResult
                };

                return PartialView(model);
            }
        }

        #endregion

        #region Email Settings

        public ActionResult EmailSettings()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
            {
                using (var db = new CaravanQLDContext())
                {
                    var currentShow = GetCurrentShow();
                    var emailSetting = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    if (emailSetting == null)
                    {
                        emailSetting = new EmailSetting();
                        emailSetting.LinkedShowId = currentShow.Id;
                        db.EmailSettings.Add(emailSetting);
                        db.SaveChanges();
                    }

                    var model = new EmailSettingsModel()
                    {
                        EmailSetting = emailSetting
                    };

                    return View(model);
                }
            }
        }

        [HttpPost]
        public ActionResult EmailSettings(EmailSettingsModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new CaravanQLDContext())
                {
                    var currentShow = GetCurrentShow();
                    var es = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    es.CompanyName = model.EmailSetting.CompanyName;
                    es.ContactEmail = model.EmailSetting.ContactEmail;
                    es.ContactNumber = model.EmailSetting.ContactNumber;
                    es.ContactFax = model.EmailSetting.ContactFax;
                    es.ContactPostalAddress = model.EmailSetting.ContactPostalAddress;
                    es.Website = model.EmailSetting.Website;
                    es.ShowTitle = model.EmailSetting.ShowTitle;
                    es.TestEmailAddress = model.EmailSetting.TestEmailAddress;
                    es.TestExhibitorShowID = model.EmailSetting.TestExhibitorShowID;
                    es.LogsPerPage = model.EmailSetting.LogsPerPage;
                    es.AdminEmailAddress = model.EmailSetting.AdminEmailAddress;
                    es.EmailSender = model.EmailSetting.EmailSender;
                    db.SaveChanges();

                    ModelState.Clear();
                }
            }

            return View(model);
        }

        #endregion

        #region Pages

        public ActionResult Pages()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return View();
        }

        public ActionResult PageList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var pages = db.ContentPages.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ThenBy(x => x.Name).ToList();
                var model = new PageListModel()
                {
                    ContentPages = pages
                };

                return PartialView(model);
            }
        }

        public ActionResult AddNewPage()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var lastPage = db.ContentPages.Where(x => x.LinkedShowId == currentShow.Id).Where(x => x.PageType == PageType.Custom).OrderByDescending(x => x.Order).FirstOrDefault();
                
                var page = new ContentPage()
                {
                    Order = lastPage != null ? lastPage.Order + 1 : 0,
                    LinkedShowId = currentShow.Id
                };

                db.ContentPages.Add(page);
                db.SaveChanges();

                var model = new EditPageModel()
                {
                    ContentPage = page
                };

                return PartialView("EditPage", model);
            }
        }

        public ActionResult EditPage(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var page = db.ContentPages.FirstOrDefault(x => x.Id == id);

                var model = new EditPageModel()
                {
                    ContentPage = page
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditPage(EditPageModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var contentPages = db.ContentPages.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var page = db.ContentPages.FirstOrDefault(x => x.Id == model.ContentPage.Id);
                page.Name = model.ContentPage.Name;
                page.IsCurrent = model.ContentPage.IsCurrent;
                page.TextContent = model.ContentPage.TextContent;

                if (page.PageType != model.ContentPage.PageType && (page.PageType == PageType.Custom && model.ContentPage.PageType != PageType.Custom))
                {
                    var oldOrder = page.Order;

                    var pagesToAdjust = contentPages.Where(x => x.Order > oldOrder && x.PageType == PageType.Custom).ToList();
                    foreach (var p in pagesToAdjust)
                    {
                        p.Order = p.Order - 1;
                    }

                    page.Order = 999;
                }
                else if (page.Order != model.ContentPage.Order || (page.PageType != model.ContentPage.PageType && model.ContentPage.PageType == PageType.Custom))
                {
                    var oldOrder = page.Order;
                    var newOrder = model.ContentPage.Order;
                    var maxOrder = contentPages.Where(x => x.PageType == PageType.Custom).Select(x => x.Order).Max();

                    if (newOrder < 0)
                        newOrder = 0;

                    if (newOrder > maxOrder)
                        newOrder = maxOrder;

                    //it already was a custom page
                    if (page.PageType == PageType.Custom)
                    {
                        if (newOrder > oldOrder)
                        {
                            var pagesToAdjust = contentPages.Where(x => x.Order > oldOrder && x.Order <= newOrder);
                            foreach (var p in pagesToAdjust)
                            {
                                p.Order = p.Order - 1;
                            }
                        }
                        else if (newOrder < oldOrder)
                        {
                            var pagesToAdjust = contentPages.Where(x => x.Order < oldOrder && x.Order >= newOrder);
                            foreach (var p in pagesToAdjust)
                            {
                                p.Order = p.Order + 1;
                            }
                        }
                    }
                    //it is a custom page now
                    else
                    {
                        var pagesToAdjust = contentPages.Where(x => x.Order > newOrder && x.PageType == PageType.Custom).ToList();
                        foreach (var p in pagesToAdjust)
                        {
                            p.Order = p.Order + 1;
                        }
                    }

                    page.Order = newOrder;
                }

                page.PageType = model.ContentPage.PageType;
                db.SaveChanges();

                ModelState.Clear();

                model.ContentPage = page;

                return PartialView(model);
            }
        }

        public ActionResult DeletePage(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var page = db.ContentPages.FirstOrDefault(x => x.Id == id);
                if (page.PageType == PageType.Custom)
                {
                    var pagesToAdjust = db.ContentPages.Where(x => x.PageType == PageType.Custom && x.Order > page.Order && x.LinkedShowId == currentShow.Id);
                    foreach (var p in pagesToAdjust)
                    {
                        p.Order = p.Order - 1;
                    }
                }
                db.ContentPages.Remove(page);
                db.SaveChanges();

                var pages = db.ContentPages.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ThenBy(x => x.Name).ToList();
                var model = new PageListModel()
                {
                    ContentPages = pages
                };

                return PartialView("PageList", model);
            }
        }

        #endregion

        #region Portal Settings

        public ActionResult PortalSettings()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return View();
        }

        public ActionResult DeleteAllData()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitorCount = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailCount = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailLogCount = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;

                var model = new DeleteAllDataModel()
                {
                    ExhibitorCount = exhibitorCount,
                    EmailCount = emailCount,
                    EmailLogCount = emailLogCount
                };

                return PartialView(model);
            }
        }

        public ActionResult DeleteData()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var ee in exhibitorEmails)
                {
                    db.ExhibitorApplicationEmails.Remove(ee);
                }

                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var email in emails)
                {
                    db.Emails.Remove(email);
                }

                var exhibitorApplications = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var exhibitorApplication in exhibitorApplications)
                {
                    db.ExhibitorApplications.Remove(exhibitorApplication);
                }

                db.SaveChanges();

                var exhibitorCount = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailCount = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailLogCount = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;

                var model = new DeleteAllDataModel()
                {
                    ExhibitorCount = exhibitorCount,
                    EmailCount = emailCount,
                    EmailLogCount = emailLogCount
                };

                return PartialView("DeleteAllData", model);
            }
        }

        public ActionResult DeleteExhibitors()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var ee in exhibitorEmails)
                {
                    db.ExhibitorApplicationEmails.Remove(ee);
                }

                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                var exhibitorApplications = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var exhibitorApplication in exhibitorApplications)
                {
                    db.ExhibitorApplications.Remove(exhibitorApplication);
                }

                db.SaveChanges();

                var exhibitorCount = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailCount = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailLogCount = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;

                var model = new DeleteAllDataModel()
                {
                    ExhibitorCount = exhibitorCount,
                    EmailCount = emailCount,
                    EmailLogCount = emailLogCount
                };

                return PartialView("DeleteAllData", model);
            }
        }

        public ActionResult DeleteEmails()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var ee in exhibitorEmails)
                {
                    db.ExhibitorApplicationEmails.Remove(ee);
                }

                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var email in emails)
                {
                    db.Emails.Remove(email);
                }

                db.SaveChanges();

                var exhibitorCount = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailCount = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailLogCount = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;

                var model = new DeleteAllDataModel()
                {
                    ExhibitorCount = exhibitorCount,
                    EmailCount = emailCount,
                    EmailLogCount = emailLogCount
                };

                return PartialView("DeleteAllData", model);
            }
        }

        #endregion 

        #region Forms

        public ActionResult Forms()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return View();
        }

        public ActionResult FormList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();

                var model = new FormListModel()
                {
                    FormGroups = formGroups,
                    Forms = forms
                };

                return PartialView(model);
            }
        }

        public ActionResult CreateFormGroup()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var lastFormGroup = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).OrderByDescending(x => x.Order).FirstOrDefault();
                var formGroup = new FormGroup();
                formGroup.LinkedShowId = currentShow.Id;

                if (lastFormGroup != null)
                    formGroup.Order = lastFormGroup.Order + 1;

                db.FormGroups.Add(formGroup);
                db.SaveChanges();

                var model = new EditFormGroupModel()
                {
                    FormGroup = formGroup
                };

                return PartialView("EditFormGroup", model);
            }
        }

        public ActionResult EditFormGroup(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formGroup = db.FormGroups.FirstOrDefault(x => x.Id == id);

                var model = new EditFormGroupModel()
                {
                    FormGroup = formGroup
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult EditFormGroup(EditFormGroupModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var formGroup = formGroups.FirstOrDefault(x => x.Id == model.FormGroup.Id);

                formGroup.Name = model.FormGroup.Name;
                
                if (formGroup.Order != model.FormGroup.Order)
                {
                    var oldOrder = formGroup.Order;
                    var newOrder = model.FormGroup.Order;
                    var maxOrder = formGroups.Select(x => x.Order).Max();

                    if (newOrder >= 0 && newOrder <= maxOrder)
                    {
                        if (newOrder > oldOrder)
                        {
                            var groupsToAdjust = formGroups.Where(x => x.Order > oldOrder && x.Order <= newOrder);
                            foreach (var fg in groupsToAdjust)
                            {
                                fg.Order = fg.Order - 1;
                            }
                        }
                        else if (newOrder < oldOrder)
                        {
                            var groupsToAdjust = formGroups.Where(x => x.Order < oldOrder && x.Order >= newOrder);
                            foreach (var fg in groupsToAdjust)
                            {
                                fg.Order = fg.Order + 1;
                            }
                        }
                    }
                    else if (newOrder < 0 && oldOrder != 0)
                    {
                        newOrder = 0;
                        var groupsToAdjust = formGroups.Where(x => x.Order < oldOrder && x.Order >= newOrder);
                        foreach (var fg in groupsToAdjust)
                        {
                            fg.Order = fg.Order + 1;
                        }
                    }
                    else if (newOrder > maxOrder && newOrder != maxOrder)
                    {
                        newOrder = maxOrder;
                        var groupsToAdjust = formGroups.Where(x => x.Order > oldOrder && x.Order <= newOrder);
                        foreach (var fg in groupsToAdjust)
                        {
                            fg.Order = fg.Order - 1;
                        }
                    }

                    formGroup.Order = newOrder;
                }

                db.SaveChanges();

                model.FormGroup = formGroup;
                //This next line tells the view to refresh all of the fields - the alternative is ModelState.Remove("FormGroup.Order");
                ModelState.Clear();

                return PartialView(model);
            }
        }

        public ActionResult DeleteFormGroup(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                //Gotta delete all the forms here etc
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var formGroup = db.FormGroups.FirstOrDefault(x => x.Id == id);

                var maxOrder = formGroups.Select(x => x.Order).Max();
                if (formGroup.Order != maxOrder)
                {
                    var groupsToAdjust = formGroups.Where(x => x.Order > formGroup.Order);
                    foreach (var fg in groupsToAdjust)
                    {
                        fg.Order = fg.Order - 1;
                    }
                }

                db.FormGroups.Remove(formGroup);
                db.SaveChanges();

                formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();

                var model = new FormListModel()
                {
                    FormGroups = formGroups,
                    Forms = forms
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult MoveFormGroupUp(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id);
                var formGroup = formGroups.FirstOrDefault(x => x.Id == id);
                var currentOrder = formGroup.Order;
                if (currentOrder != 0)
                {
                    var newOrder = formGroup.Order - 1;
                    var formGroupAbove = formGroups.FirstOrDefault(x => x.Order == newOrder);
                    formGroup.Order = newOrder;
                    formGroupAbove.Order = currentOrder;
                    db.SaveChanges();
                }

                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();

                var model = new FormListModel()
                {
                    FormGroups = formGroups.OrderBy(x => x.Order).ToList(),
                    Forms = forms
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult MoveFormGroupDown(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id);
                var formGroup = formGroups.FirstOrDefault(x => x.Id == id);
                var currentOrder = formGroup.Order;
                var maxOrder = formGroups.Select(x => x.Order).Max();
                if (currentOrder != maxOrder)
                {
                    var newOrder = formGroup.Order + 1;
                    var formGroupAbove = formGroups.FirstOrDefault(x => x.Order == newOrder);
                    formGroup.Order = newOrder;
                    formGroupAbove.Order = currentOrder;
                    db.SaveChanges();
                }

                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();

                var model = new FormListModel()
                {
                    FormGroups = formGroups.OrderBy(x => x.Order).ToList(),
                    Forms = forms
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult CreateForm()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formGroup = db.FormGroups.Include("Forms").Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).FirstOrDefault();
                if (formGroup == null)
                {
                    formGroup = new FormGroup();
                    db.FormGroups.Add(formGroup);
                }

                var formGroupOrder = 0;
                
                if (formGroup.Forms.Count >= 1)
                    formGroupOrder = formGroup.Forms.Select(x => x.Order).Max() + 1;

                var form = new Form()
                {
                    Order = formGroupOrder,
                    FormGroupId = formGroup.Id,
                    LinkedShowId = currentShow.Id
                };

                db.Forms.Add(form);
                db.SaveChanges();

                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Name).ToList().Select(x => 
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }).ToList();

                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Subject).ToList().Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Subject,
                        Value = x.Id.ToString()
                    }).ToList();

                emails.Insert(0, new SelectListItem() { Text = "", Value = "null" });

                var model = new EditFormModel()
                {
                    Form = form,
                    FormGroups = formGroups,
                    FormTypes = CreateFormTypeList(),
                    Emails = emails
                };

                return PartialView("EditForm", model);
            }
        }

        public ActionResult EditForm(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var form = db.Forms.FirstOrDefault(x => x.Id == id);
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Name).ToList().Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }).ToList();

                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Subject).ToList().Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Subject,
                        Value = x.Id.ToString()
                    }).ToList();

                emails.Insert(0, new SelectListItem() { Text = "", Value = "null" });

                var model = new EditFormModel()
                {
                    Form = form,
                    FormGroups = formGroups,
                    FormTypes = CreateFormTypeList(),
                    Emails = emails
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditForm(EditFormModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Name).ToList();
                var form = forms.FirstOrDefault(x => x.Id == model.Form.Id);
                form.Name = model.Form.Name;
                if (form.Order != model.Form.Order && form.FormGroupId == model.Form.FormGroupId)
                {
                    var oldOrder = form.Order;
                    var newOrder = model.Form.Order;
                    if (oldOrder > newOrder)
                    {
                        if (newOrder < 0)
                            newOrder = 0;

                        var formsToAdjust = forms.Where(x => x.FormGroupId == form.FormGroupId && x.Order < oldOrder && x.Order >= newOrder);
                        foreach (var f in formsToAdjust)
                        {
                            f.Order = f.Order + 1;
                        }
                    }
                    else if (oldOrder < newOrder)
                    {
                        var maxOrder = forms.Where(x => x.FormGroupId == form.FormGroupId).Select(x => x.Order).Max();
                        if (newOrder > maxOrder)
                            newOrder = maxOrder;

                        var formsToAdjust = forms.Where(x => x.FormGroupId == form.FormGroupId && x.Order > oldOrder && x.Order <= newOrder);
                        foreach (var f in formsToAdjust)
                        {
                            f.Order = f.Order - 1;
                        }
                    }

                    form.Order = newOrder;
                }
                else if (form.FormGroupId != model.Form.FormGroupId)
                {
                    var oldFormGroup = formGroups.FirstOrDefault(x => x.Id == form.FormGroupId);
                    var oldOrder = form.Order;
                    var newFormGroup = formGroups.FirstOrDefault(x => x.Id == model.Form.FormGroupId);
                    var newOrder = model.Form.Order;
                    //first deal with the old form group
                    var oldFormsToAdjust = forms.Where(x => x.FormGroupId == oldFormGroup.Id && x.Order >= oldOrder);
                    foreach (var f in oldFormsToAdjust)
                    {
                        f.Order = f.Order - 1;
                    }

                    //then deal with the new form group
                    if (newOrder < 0)
                        newOrder = 0;
                    else if (forms.Where(x => x.FormGroupId == newFormGroup.Id).ToList().Count <= 0)
                    {
                        newOrder = 0;
                    }
                    else
                    {
                        var maxOrder = forms.Where(x => x.FormGroupId == newFormGroup.Id).Select(x => x.Order).Max();
                        if (newOrder > maxOrder)
                            newOrder = maxOrder;
                    }

                    var newFormsToAdjust = forms.Where(x => x.FormGroupId == newFormGroup.Id && x.Order >= newOrder);
                    foreach (var f in newFormsToAdjust)
                    {
                        f.Order = f.Order + 1;
                    }

                    form.Order = newOrder;
                }

                form.DueDate = model.Form.DueDate;
                form.Type = model.Form.Type;
                form.Mandatory = model.Form.Mandatory;
                form.IsCurrent = model.Form.IsCurrent;
                form.FormGroupId = model.Form.FormGroupId;
                form.TextContent = model.Form.TextContent;
                form.EmailId = model.Form.EmailId;
                db.SaveChanges();

                ModelState.Clear();

                var formGroupList = formGroups.OrderBy(x => x.Name).ToList().Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }).ToList();

                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Subject).ToList().Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Subject,
                        Value = x.Id.ToString()
                    }).ToList();

                emails.Insert(0, new SelectListItem() { Text = "", Value = "null" });

                model = new EditFormModel()
                {
                    Form = form,
                    FormGroups = formGroupList,
                    FormTypes = CreateFormTypeList(),
                    Emails = emails
                };

                return PartialView(model);
            }
        }

        private List<SelectListItem> CreateFormTypeList()
        {
            var typeList = new List<SelectListItem>();
            typeList.Add(new SelectListItem()
            {
                Text = "Standard",
                Value = "Standard"
            });

            typeList.Add(new SelectListItem()
            {
                Text = "Content",
                Value = "Content"
            });

            return typeList;
        }

        public ActionResult DeleteForm(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                //what other things do I need to delete?
                var form = db.Forms.FirstOrDefault(x => x.Id == id);
                db.Forms.Remove(form);
                db.SaveChanges();

                var currentShow = GetCurrentShow();
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();

                var model = new FormListModel()
                {
                    FormGroups = formGroups,
                    Forms = forms
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult MoveFormUp(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var form = forms.FirstOrDefault(x => x.Id == id);
                var formGroups = db.FormGroups.Include("Forms").Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();

                var currentOrder = form.Order;
                if (currentOrder != 0)
                {
                    var newOrder = currentOrder - 1;
                    var formAbove = forms.FirstOrDefault(x => x.Order == newOrder && x.FormGroupId == form.FormGroupId);
                    form.Order = newOrder;
                    formAbove.Order = currentOrder;
                }
                else
                {
                    var currentFormGroup = formGroups.FirstOrDefault(x => x.Id == form.FormGroupId);
                    if (currentFormGroup.Order != 0)
                    {
                        var formGroupAbove = formGroups.FirstOrDefault(x => x.Order == currentFormGroup.Order - 1);
                        var maxOrder = 0;
                        if (formGroupAbove.Forms.Count > 0)
                            maxOrder = formGroupAbove.Forms.Select(x => x.Order).Max() + 1;

                        form.Order = maxOrder;
                        form.FormGroupId = formGroupAbove.Id;

                        foreach (var f in forms.Where(x => x.FormGroupId == currentFormGroup.Id))
                        {
                            f.Order = f.Order - 1;
                        }
                    }
                }

                db.SaveChanges();

                var model = new FormListModel()
                {
                    FormGroups = formGroups,
                    Forms = forms.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult MoveFormDown(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var form = forms.FirstOrDefault(x => x.Id == id);
                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();

                var maxOrder = forms.Where(x => x.FormGroupId == form.FormGroupId).Select(x => x.Order).Max();
                if (form.Order != maxOrder)
                {
                    var currentOrder = form.Order;
                    var newOrder = currentOrder + 1;
                    var formBelow = forms.FirstOrDefault(x => x.Order == newOrder && x.FormGroupId == form.FormGroupId);
                    form.Order = newOrder;
                    formBelow.Order = currentOrder;
                }
                else
                {
                    var maxFormGroupOrder = formGroups.Select(x => x.Order).Max();
                    var currentFormGroup = formGroups.FirstOrDefault(x => x.Id == form.FormGroupId);

                    if (currentFormGroup.Order != maxFormGroupOrder)
                    {
                        var formGroupBelow = formGroups.FirstOrDefault(x => x.Order == currentFormGroup.Order + 1);

                        foreach (var f in forms.Where(x => x.FormGroupId == formGroupBelow.Id))
                        {
                            f.Order = f.Order + 1;
                        }

                        form.Order = 0;
                        form.FormGroupId = formGroupBelow.Id;
                    }

                }

                db.SaveChanges();

                var model = new FormListModel()
                {
                    FormGroups = formGroups,
                    Forms = forms.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult EditStandardForm(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var form = db.Forms.Include("FormFields").FirstOrDefault(x => x.Id == id);
                var model = new EditStandardFormModel()
                {
                    Form = form
                };

                return PartialView(model);
            }
        }

        public ActionResult CreateFormField(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var form = db.Forms.Include("FormFields").FirstOrDefault(x => x.Id == id);
                var formField = new FormField()
                {
                    FormId = id,
                    Order = form.FormFields.ToList().Count
                };
                db.FormFields.Add(formField);
                db.SaveChanges();

                var model = new EditFormFieldModel()
                {
                    FormField = formField
                };

                return PartialView("EditFormField", model);
            }
        }

        public ActionResult EditFormField(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                var model = new EditFormFieldModel()
                {
                    FormField = formField
                };
                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditFormField(EditFormFieldModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == model.FormField.Id);
                var formFields = db.FormFields.Where(x => x.FormId == formField.FormId).ToList();
                formField.Name = model.FormField.Name;
                formField.Label = model.FormField.Label;
                formField.Mandatory = model.FormField.Mandatory;
                formField.DataType = model.FormField.DataType;

                if (formField.Order != model.FormField.Order)
                {
                    var currentOrder = formField.Order;
                    var newOrder = model.FormField.Order;

                    if (currentOrder > newOrder)
                    {
                        if (newOrder < 0)
                            newOrder = 0;

                        var formFieldsToAdjust = formFields.Where(x => x.Order >= newOrder && x.Order < currentOrder);
                        foreach (var f in formFieldsToAdjust)
                        {
                            f.Order = f.Order + 1;
                        }
                    }
                    else if (currentOrder < newOrder)
                    {
                        if (formFields.Count > 0)
                        {
                            var maxOrder = formFields.Select(x => x.Order).Max();
                            if (newOrder > maxOrder)
                                newOrder = maxOrder;
                        }

                        var formFieldsToAdjust = formFields.Where(x => x.Order <= newOrder && x.Order > currentOrder);
                        foreach (var f in formFieldsToAdjust)
                        {
                            f.Order = f.Order - 1;
                        }
                    }

                    formField.Order = newOrder;
                }

                formField.SpanTwoColumns = model.FormField.SpanTwoColumns;
                if (model.FormField.DataType == FormDataType.ContentSection)
                {
                    formField.TextContent = model.FormField.TextContent;
                }

                db.SaveChanges();
                ModelState.Clear();
                model.FormField = formField;

                return PartialView(model);
            }
        }

        public ActionResult FormFieldList(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var form = db.Forms.FirstOrDefault(x => x.Id == id);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == id).OrderBy(x => x.Order).ToList();
                var model = new FormFieldListModel()
                {
                    Form = form,
                    FormFields = formFields
                };

                return PartialView(model);
            }
        }

        public ActionResult MoveFormFieldUp(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == formField.FormId).ToList();

                if (formField.Order != 0)
                {
                    var currentOrder = formField.Order;
                    var newOrder = formField.Order - 1;
                    var formFieldAbove = formFields.FirstOrDefault(x => x.Order == newOrder);

                    formField.Order = newOrder;
                    formFieldAbove.Order = currentOrder;
                    db.SaveChanges();
                }

                var model = new FormFieldListModel()
                {
                    Form = formField.Form,
                    FormFields = formFields.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormFieldList", model);
            }
        }

        public ActionResult MoveFormFieldDown(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == formField.FormId).ToList();

                var maxOrder = 0;
                if (formFields.Count > 0)
                    maxOrder = formFields.Select(x => x.Order).Max();

                if (formField.Order < maxOrder)
                {
                    var currentOrder = formField.Order;
                    var newOrder = formField.Order + 1;
                    var formFieldBelow = formFields.FirstOrDefault(x => x.Order == newOrder);

                    formField.Order = newOrder;
                    formFieldBelow.Order = currentOrder;
                    db.SaveChanges();
                }

                var model = new FormFieldListModel()
                {
                    Form = formField.Form,
                    FormFields = formFields.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormFieldList", model);
            }
        }

        public ActionResult GetNewFormFieldOrder(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                return Json(formField.Order, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteFormField(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                var form = db.Forms.FirstOrDefault(x => x.Id == formField.FormId);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == formField.FormId);
                var formFieldsToAdjust = formFields.Where(x => x.Order > formField.Order).ToList();

                db.FormFields.Remove(formField);

                foreach (var f in formFieldsToAdjust)
                {
                    f.Order = f.Order - 1;
                }

                db.SaveChanges();

                var model = new FormFieldListModel()
                {
                    Form = form,
                    FormFields = formFields.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormFieldList", model);
            }
        }

        public ActionResult CreateFormFieldItem(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.Include("FormFieldItems").FirstOrDefault(x => x.Id == id);
                var maxOrder = 0;
                if (formField.FormFieldItems.Count > 0)
                    maxOrder = formField.FormFieldItems.Select(x => x.Order).Max() + 1;

                var formFieldItem = new FormFieldItem()
                {
                    FormFieldId = id,
                    Order = maxOrder
                };

                db.FormFieldItems.Add(formFieldItem);
                db.SaveChanges();

                var model = new EditFormFieldItemModel()
                {
                    FormFieldItem = formFieldItem
                };

                return PartialView("EditFormFieldItem", model);
            }
        }

        public ActionResult EditFormFieldItem(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formFieldItem = db.FormFieldItems.FirstOrDefault(x => x.Id == id);
                var model = new EditFormFieldItemModel()
                {
                    FormFieldItem = formFieldItem
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult EditFormFieldItem(EditFormFieldItemModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var formFieldItem = db.FormFieldItems.FirstOrDefault(x => x.Id == model.FormFieldItem.Id);
                var formFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == formFieldItem.FormFieldId).ToList();
                formFieldItem.Text = model.FormFieldItem.Text;
                formFieldItem.Value = model.FormFieldItem.Value;

                if (formFieldItem.Order != model.FormFieldItem.Order)
                {
                    var currentOrder = formFieldItem.Order;
                    var newOrder = model.FormFieldItem.Order;

                    if (currentOrder > newOrder)
                    {
                        if (newOrder < 0)
                            newOrder = 0;

                        var formFieldItemsToAdjust = formFieldItems.Where(x => x.Order >= newOrder && x.Order < currentOrder);
                        foreach (var f in formFieldItemsToAdjust)
                        {
                            f.Order = f.Order + 1;
                        }
                    }
                    else if (currentOrder < newOrder)
                    {
                        if (formFieldItems.Count > 0)
                        {
                            var maxOrder = formFieldItems.Select(x => x.Order).Max();
                            if (newOrder > maxOrder)
                                newOrder = maxOrder;
                        }

                        var formFieldItemsToAdjust = formFieldItems.Where(x => x.Order <= newOrder && x.Order > currentOrder);
                        foreach (var f in formFieldItemsToAdjust)
                        {
                            f.Order = f.Order - 1;
                        }
                    }

                    formFieldItem.Order = newOrder;
                }

                db.SaveChanges();
                model.FormFieldItem = formFieldItem;
                ModelState.Clear();

                return PartialView(model);
            }
        }

        public ActionResult FormFieldItemList(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.Include("FormFieldItems").FirstOrDefault(x => x.Id == id);
                var model = new FormFieldItemListModel()
                {
                    FormFieldItems = formField.FormFieldItems.OrderBy(x => x.Order).ToList(),
                    FormField = formField
                };
                return PartialView(model);
            }
        }

        public ActionResult DeleteFormFieldItem(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formFieldItem = db.FormFieldItems.Include("FormField").FirstOrDefault(x => x.Id == id);
                var formField = formFieldItem.FormField;
                var formFieldItemsToAdjust = db.FormFieldItems.Where(x => x.FormFieldId == formFieldItem.FormFieldId && x.Order > formFieldItem.Order);

                foreach (var f in formFieldItemsToAdjust)
                {
                    f.Order = f.Order - 1;
                }

                db.FormFieldItems.Remove(formFieldItem);
                db.SaveChanges();

                var model = new FormFieldItemListModel()
                {
                    FormFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == formField.Id).OrderBy(x => x.Order).ToList(),
                    FormField = formField
                };

                return PartialView("FormFieldItemList", model);
            }
        }

        #endregion

        #region Form Data/Statistics

        public ActionResult ViewFormData(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var form = db.Forms.FirstOrDefault(x => x.Id == id);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var formExhibitors = db.FormExhibitors.Where(x => x.FormId == form.Id).ToList();
                var completedExhibitors = exhibitors.Where(x => formExhibitors.Select(z => z.ExhibitorApplicationId).Contains(x.Id)).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var uncompletedExhibitors = exhibitors.Where(x => !formExhibitors.Select(z => z.ExhibitorApplicationId).Contains(x.Id)).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var emailLogs = new List<EmailLog>();
                if (form.EmailId != null)
                {
                    emailLogs = db.EmailLogs.Where(x => x.EmailId == form.EmailId).ToList();
                }

                var model = new ViewFormDataModel()
                {
                    Form = form,
                    CompletedExhibitors = completedExhibitors,
                    UncompletedExhibitors = uncompletedExhibitors,
                    FormExhibitors = formExhibitors,
                    EmailLogsForForm = emailLogs
                };

                return PartialView(model);
            }
        }

        public ActionResult SendReminderEmail(Guid FormId, Guid ExhibitorId)
        {
            var success = true;

            using (var db = new CaravanQLDContext())
            {
                try
                {
                    var currentShow = GetCurrentShow();
                    var form = db.Forms.FirstOrDefault(x => x.Id == FormId);
                    var email = db.Emails.FirstOrDefault(x => x.Id == form.EmailId);
                    var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == ExhibitorId);
                    var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
#if Debug
                        SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
#else
                    SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                    client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
#endif
                    MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                    MailAddress to = new MailAddress(exhibitor.Email);
                    MailMessage message = new MailMessage(from, to);
                    var newBody = AppMethods.ReplaceSmartFields(exhibitor, email.Body, emailSettings) + GetEmailFooter();
                    message.Body = newBody;
                    message.Subject = email.Subject;
                    message.IsBodyHtml = true;
                    message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    client.Send(message);

                    var emailLog = new EmailLog()
                    {
                        DateSent = DateTime.Now,
                        EmailId = (Guid)form.EmailId,
                        ExhibitorApplicationId = ExhibitorId
                    };

                    db.EmailLogs.Add(emailLog);
                    db.SaveChanges();
                }
                catch (Exception exc)
                {
                    success = false;
                }

                //Triggers the failure message or Triggers the success message
                if (!success)
                    return Json(new { success = false });
                else
                {
                    var currentShow = GetCurrentShow();
                    var form = db.Forms.FirstOrDefault(x => x.Id == FormId);
                    var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                    var formExhibitors = db.FormExhibitors.Where(x => x.FormId == form.Id).ToList();
                    var completedExhibitors = exhibitors.Where(x => formExhibitors.Select(z => z.ExhibitorApplicationId).Contains(x.Id)).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                    var uncompletedExhibitors = exhibitors.Where(x => !formExhibitors.Select(z => z.ExhibitorApplicationId).Contains(x.Id)).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                    var emailLogs = new List<EmailLog>();
                    if (form.EmailId != null)
                    {
                        emailLogs = db.EmailLogs.Where(x => x.EmailId == form.EmailId).ToList();
                    }
                    var model = new ViewFormDataModel()
                    {
                        Form = form,
                        CompletedExhibitors = completedExhibitors,
                        UncompletedExhibitors = uncompletedExhibitors,
                        FormExhibitors = formExhibitors,
                        EmailLogsForForm = emailLogs
                    };
                    return PartialView("ViewFormData", model);
                }
            }
        }

        public ActionResult ViewFormFieldData(Guid formId, Guid exhibitorId)
        {
            using (var db = new CaravanQLDContext())
            {
                var form = db.Forms.Include("FormFields").FirstOrDefault(x => x.Id == formId);
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == exhibitorId);
                var formFields = db.FormFields.Where(x => x.FormId == form.Id).OrderBy(x => x.Order).ToList();
                var formFieldIds = formFields.Select(x => x.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id && formFieldIds.Contains(x.FormFieldId)).ToList();
                var model = new ViewFormFieldDataModel()
                {
                    FormFields = formFields,
                    FormFieldValues = formFieldValues,
                    Exhibitor = exhibitor
                };

                return PartialView(model);
            }
        }

        public ActionResult ViewFormFieldDataHeaderRow(Guid formId)
        {
            using (var db = new CaravanQLDContext())
            {
                var form = db.Forms.Include("FormFields").FirstOrDefault(x => x.Id == formId);
                var formFields = db.FormFields.Where(x => x.FormId == form.Id).OrderBy(x => x.Order).ToList();
                var model = new ViewFormFieldDataModel()
                {
                    FormFields = formFields
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult ViewFormDataSearch(Guid formId)
        {
            using (var db = new CaravanQLDContext())
            {
                var request = Request;
                var searchField = "";
                var searchTerm = "";

                var currentShow = GetCurrentShow();
                var form = db.Forms.FirstOrDefault(x => x.Id == formId);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var formExhibitors = db.FormExhibitors.Where(x => x.FormId == form.Id).ToList();
                var completedExhibitors = exhibitors.Where(x => formExhibitors.Select(z => z.ExhibitorApplicationId).Contains(x.Id)).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var searchResults = new List<ExhibitorApplication>();

                var tradingNameSearch = request.Form.Get("exhibitors");
                if (!String.IsNullOrEmpty(tradingNameSearch))
                {
                    searchField = "exhibitors";
                    searchTerm = tradingNameSearch;
                    searchResults = completedExhibitors.Where(x => x.TradingName.ToLower().Contains(tradingNameSearch.ToLower())).ToList();
                }

                var dateViewedSearch = request.Form.Get("dateViewed");
                if (!String.IsNullOrEmpty(dateViewedSearch))
                {
                    searchField = "dateViewed";
                    searchTerm = dateViewedSearch;
                    var searchDate = DateTime.Parse(dateViewedSearch);
                    searchResults = exhibitors.Where(x => formExhibitors.Where(y => y.DateCompleted >= searchDate && y.DateCompleted < searchDate.AddDays(1)).Select(z => z.ExhibitorApplicationId).Contains(x.Id)).ToList();
                }

                var formFields = db.FormFields.Where(x => x.FormId == form.Id).OrderBy(x => x.Order).ToList();
                foreach (var ff in formFields)
                {
                    var formFieldSearch = request.Form.Get(ff.Id.ToString());
                    if (!String.IsNullOrEmpty(formFieldSearch))
                    {
                        searchField = ff.Id.ToString();
                        searchTerm = formFieldSearch;
                        var formFieldValueIds = db.FormFieldValues.Where(x => x.FormFieldId == ff.Id && x.Value.ToLower().Contains(formFieldSearch.ToLower())).Select(x => x.ExhibitorApplicationId).ToList();
                        searchResults = completedExhibitors.Where(x => formFieldValueIds.Contains(x.Id)).ToList();
                        break;
                    }
                }

                var model = new ViewFormDataSearchModel()
                {
                    Form = form,
                    CompletedExhibitors = searchResults,
                    FormExhibitors = formExhibitors,
                    SearchField = searchField,
                    SearchTerm = searchTerm
                };

                return PartialView(model);
            }
        }

        public ActionResult ExportFormData(Guid formId, string exhibitorsToExport)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var form = db.Forms.FirstOrDefault(x => x.Id == formId);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var formExhibitors = db.FormExhibitors.Where(x => x.FormId == form.Id).ToList();
                var formFields = db.FormFields.Where(x => x.FormId == form.Id && x.DataType != FormDataType.ContentSection).OrderBy(x => x.Order).ToList();
                var completedExhibitors = new List<ExhibitorApplication>();

                //check to see if this export is for certain exhibitors only
                if (!String.IsNullOrEmpty(exhibitorsToExport))
                {
                    //get only the selected exhibitors
                    var selectedExhibitorIds = exhibitorsToExport.Split('|');
                    var idList = new List<Guid>();
                    foreach (var stringId in selectedExhibitorIds)
                    {
                        idList.Add(new Guid(stringId));
                    }

                    completedExhibitors = exhibitors.Where(x => idList.Contains(x.Id)).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                }
                else
                {
                    //get all completed exhibitors
                    completedExhibitors = exhibitors.Where(x => formExhibitors.Select(z => z.ExhibitorApplicationId).Contains(x.Id)).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                }

                var fileName = String.Format("{0} {1}.csv", form.Name, DateTime.Now.ToString("dd-MM-yyyy hh-mm-ss"));
                var filePath = Path.Combine(Request.PhysicalApplicationPath, "Files", "files", fileName);
                using (var file = System.IO.File.Open(filePath, FileMode.CreateNew))
                {
                    using (var csvWriter = new CsvWriter(file))
                    {
                        string[] headerFields = new string[formFields.Count + 6];
                        headerFields[0] = "Exhibitor";
                        headerFields[1] = "Show ID";
                        headerFields[2] = "Show Listing Name";
                        headerFields[3] = "Stand No";
                        headerFields[4] = "Location";
                        headerFields[5] = form.Type == FormType.Content ? "Date Viewed" : "Date Completed";

                        for (int i = 0; i < formFields.Count; i++)
                        {
                            headerFields[i + 6] = formFields[i].Name;
                        }

                        csvWriter.Write(headerFields);

                        foreach (var exhibitor in completedExhibitors)
                        {
                            var formFieldValues = db.FormFieldValues.ToList().Where(x => formFields.Select(ff => ff.Id).Contains(x.FormFieldId) && x.ExhibitorApplicationId == exhibitor.Id).ToList();

                            try
                            {
                                string[] rowFields = new string[formFields.Count + 6];
                                rowFields[0] = !String.IsNullOrEmpty(exhibitor.TradingName) ? exhibitor.TradingName : exhibitor.ExhibitorEmail;
                                rowFields[1] = exhibitor.ShowId;
                                rowFields[2] = exhibitor.ShowListingName;
                                rowFields[3] = exhibitor.AllocatedStandNumber;
                                rowFields[4] = exhibitor.SiteLocation;
                                rowFields[5] = ((DateTime)formExhibitors.FirstOrDefault(x => x.ExhibitorApplicationId == exhibitor.Id).DateCompleted).ToString("dd/MM/yyyy");

                                for (int i = 0; i < formFields.Count; i++)
                                {
                                    rowFields[i + 6] = formFieldValues.FirstOrDefault(x => x.FormFieldId == formFields[i].Id && x.ExhibitorApplicationId == exhibitor.Id).Value;
                                }

                                csvWriter.Write(rowFields);
                            }
                            catch (Exception exc)
                            {

                            }
                        }
                    }
                }

                return File(filePath, "application/csv", fileName);
            }
        }

        #endregion

        #region Shows

        private Show GetCurrentShow()
        {
            var showId = (Guid)Session[AppConstants.CurrentShowAdminSession];
            if (showId != null)
            {
                using (var db = new CaravanQLDContext())
                {
                    var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                    return show;
                }
            }
            else
            {
                return null;
            }
        }

        public ActionResult Shows()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else
            {
                return View();
            }
        }

        public ActionResult ShowList()
        {
            using (var db = new CaravanQLDContext())
            {
                var shows = db.Shows.OrderBy(x => x.Name).ToList();
                var model = new ShowListModel()
                {
                    Shows = shows
                };

                return PartialView(model);
            }
        }

        public ActionResult AddNewShow()
        {
            using (var db = new CaravanQLDContext())
            {
                var show = new Show();

                try
                {
                    Directory.CreateDirectory(Path.Combine(AppConstants.FilesBaseDir, "Exhibitors", show.LinkIdentifier));
                    db.Shows.Add(show);
                    db.SaveChanges();
                }
                catch (Exception exc)
                {

                }

                var model = new EditShowModel()
                {
                    Show = show
                };

                return PartialView("EditShow", model);
            }
        }

        public ActionResult EditShow(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var show = db.Shows.FirstOrDefault(x => x.Id == id);

                var model = new EditShowModel()
                {
                    Show = show
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult EditShow(EditShowModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var show = db.Shows.FirstOrDefault(x => x.Id == model.Show.Id);

                show.Name = model.Show.Name;
                show.ShowType = model.Show.ShowType;
                show.StartDate = model.Show.StartDate;
                show.EndDate = model.Show.EndDate;
                if (show.LinkIdentifier != model.Show.LinkIdentifier)
                {
                    Directory.Move(Path.Combine(AppConstants.FilesBaseDir, "Exhibitors", show.LinkIdentifier), Path.Combine(AppConstants.FilesBaseDir, "Exhibitors", model.Show.LinkIdentifier.Replace(" ", "")));
                    show.LinkIdentifier = model.Show.LinkIdentifier.Replace(" ", "");
                }

                db.SaveChanges();

                ModelState.Clear();
                model.Show = show;

                return PartialView(model);
            }
        }

        public ActionResult DeleteShow(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var show = db.Shows.FirstOrDefault(x => x.Id == id);
                var contentPages = db.ContentPages.ToList();
                foreach (var contentPage in contentPages)
                {
                    db.ContentPages.Remove(contentPage);
                }

                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == id).ToList();
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                var emails = db.Emails.Where(x => x.LinkedShowId == id).ToList();
                foreach (var email in emails)
                {
                    db.Emails.Remove(email);
                }

                var emailSettings = db.EmailSettings.Where(x => x.LinkedShowId == id).ToList();
                foreach (var emailSetting in emailSettings)
                {
                    db.EmailSettings.Remove(emailSetting);
                }

                var exhibitorApplications = db.ExhibitorApplications.Where(x => x.LinkedShowId == id).ToList();
                foreach (var ea in exhibitorApplications)
                {
                    db.ExhibitorApplications.Remove(ea);
                }

                var forms = db.Forms.Where(x => x.LinkedShowId == id).ToList();
                foreach (var form in forms)
                {
                    db.Forms.Remove(form);
                }

                var formGroups = db.FormGroups.Where(x => x.LinkedShowId == id).ToList();
                foreach (var formGroup in formGroups)
                {
                    db.FormGroups.Remove(formGroup);
                }

                db.Shows.Remove(show);
                db.SaveChanges();

                var model = new ShowListModel()
                {
                    Shows = db.Shows.OrderBy(x => x.Name).ToList()
                };

                return PartialView("ShowList", model);
            }
        }

        public ActionResult ChooseShow()
        {
            using (var db = new CaravanQLDContext())
            {
                var shows = db.Shows.OrderBy(x => x.Name).ToList();

                var model = new ChooseShowModel()
                {
                    Shows = shows
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult ChooseShow(ChooseShowModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var showId = new Guid(model.Show);
                var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                if (show != null)
                {
                    Session[AppConstants.CurrentShowAdminSession] = showId;
                    return RedirectToAction("Exhibitors");
                }
                else
                {
                    var shows = db.Shows.OrderBy(x => x.Name).ToList();
                    model = new ChooseShowModel()
                    {
                        Shows = shows
                    };

                    return View(model);
                }
            }
        }

        #endregion
    }
}
