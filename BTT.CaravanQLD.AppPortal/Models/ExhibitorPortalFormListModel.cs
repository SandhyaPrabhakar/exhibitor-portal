﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ExhibitorPortalFormListModel
    {
        public ExhibitorApplication Exhibitor { get; set; }
        public List<FormGroup> FormGroups { get; set; }
        public ContentPage Page { get; set; }
    }
}