﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class EmailsModel
    {
        public bool ForwardToEmail { get; set; }
        public Guid EmailId { get; set; }
    }
}