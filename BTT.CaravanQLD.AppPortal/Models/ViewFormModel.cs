﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ViewFormModel
    {
        public ExhibitorApplication Exhibitor { get; set; }
        public Form Form { get; set; }
        public List<FormField> FormFields { get; set; }
        public List<FormFieldValue> FormFieldValues { get; set; }
        public Show CurrentShow { get; set; }
    }
}