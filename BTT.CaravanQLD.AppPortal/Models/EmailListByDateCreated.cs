﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class EmailListByDateCreated
    {
        public List<Email> Emails { get; set; }
        public List<DateTime> Last12Months { get; set; }
    }
}