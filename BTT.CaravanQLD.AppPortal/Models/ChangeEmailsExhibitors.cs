﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ChangeEmailsExhibitors
    {
        public Email CurrentEmail { get; set; }
        public List<ExhibitorApplication> Exhibitors { get; set; }
        public List<ExhibitorApplicationEmail> ExhibitorApplicationEmails { get; set; }
        public List<EmailLog> EmailLogs { get; set; }
    }
}