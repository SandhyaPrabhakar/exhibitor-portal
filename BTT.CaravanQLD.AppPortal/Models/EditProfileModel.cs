﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class EditProfileModel
    {
        public ExhibitorApplication Exhibitor { get; set; }
        public List<SelectListItem> StateList { get; set; }
        public EmailSetting EmailSettings { get; set; }
    }
}