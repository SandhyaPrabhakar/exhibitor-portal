﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class LogListForSearchModel
    {
        public Guid SearchId { get; set; }
        public string SearchType { get; set; }
        public List<EmailLog> EmailLogs { get; set; }
    }
}