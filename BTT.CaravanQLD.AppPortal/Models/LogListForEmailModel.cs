﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class LogListForEmailModel
    {
        public Guid EmailId { get; set; }
        public List<EmailLog> EmailLogs { get; set; }
        public int CurrentPage { get; set; }
        public int TotalRecords { get; set; }
        public int LogsPerPage { get; set; }
    }
}