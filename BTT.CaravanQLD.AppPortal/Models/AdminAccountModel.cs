﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class AdminAccountModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}