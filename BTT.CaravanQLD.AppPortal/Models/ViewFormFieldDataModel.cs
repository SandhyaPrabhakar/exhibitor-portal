﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ViewFormFieldDataModel
    {
        public List<FormField> FormFields { get; set; }
        public List<FormFieldValue> FormFieldValues { get; set; }
        public ExhibitorApplication Exhibitor { get; set; }
    }
}