﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ViewFormDataSearchModel
    {
        public Form Form { get; set; }
        public List<ExhibitorApplication> CompletedExhibitors { get; set; }
        public List<FormExhibitor> FormExhibitors { get; set; }
        public string SearchField { get; set; }
        public string SearchTerm { get; set; }
    }
}