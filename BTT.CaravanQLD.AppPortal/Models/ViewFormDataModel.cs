﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ViewFormDataModel
    {
        public Form Form { get; set; }
        public List<ExhibitorApplication> CompletedExhibitors { get; set; }
        public List<ExhibitorApplication> UncompletedExhibitors { get; set; }
        public List<FormExhibitor> FormExhibitors { get; set; }
        public List<EmailLog> EmailLogsForForm { get; set; }
    }
}