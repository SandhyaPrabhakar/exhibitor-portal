﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ExhibitorListForSearchModel
    {
        public List<ExhibitorApplication> Exhibitors { get; set; }
    }
}