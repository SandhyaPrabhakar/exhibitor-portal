﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class NavigationModel
    {
        public ExhibitorApplication Exhibitor { get; set; }
        public List<ContentPage> Pages { get; set; }
    }
}