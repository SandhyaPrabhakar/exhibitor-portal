﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class FormListModel
    {
        public List<FormGroup> FormGroups { get; set; }
        public List<Form> Forms { get; set; }
    }
}