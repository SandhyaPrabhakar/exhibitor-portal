﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class DeleteAllDataModel
    {
        public int ExhibitorCount { get; set; }
        public int EmailCount { get; set; }
        public int EmailLogCount { get; set; }
    }
}