﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ViewExhibitorFormStatsModel
    {
        public ExhibitorApplication Exhibitor { get; set; }
        public List<Form> CompletedForms { get; set; }
        public List<Form> UncompletedForms { get; set; }
        public List<FormExhibitor> FormExhibitors { get; set; }
        public List<EmailLog> EmailLogsForExhibitor { get; set; }
    }
}