﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class AccountModel
    {
        public string ShowID { get; set; }
        public string CompanyEmail { get; set; }
        public ContentPage Page { get; set; }
    }
}