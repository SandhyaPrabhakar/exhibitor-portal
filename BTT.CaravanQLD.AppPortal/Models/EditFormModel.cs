﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class EditFormModel
    {
        public Form Form { get; set; }
        public List<SelectListItem> FormGroups { get; set; }
        public List<SelectListItem> FormTypes { get; set; }
        public List<SelectListItem> Emails { get; set; }
    }
}