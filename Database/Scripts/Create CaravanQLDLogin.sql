use Master
go

if not exists (select * from master.dbo.syslogins where [Name] = 'CaravanQLDLogin')
  exec sp_addlogin 
      @loginame = 'CaravanQLDLogin' 
    , @passwd = 'Admin@CaravanQLD'
  EXEC master..sp_addsrvrolemember @loginame = N'CaravanQLDLogin', @rolename = N'sysadmin'
go